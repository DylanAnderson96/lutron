//
//  CasetaRooms.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/15/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class CasetaRoom: NSObject {
    
    struct Keys {
        static let created = "created"
        static let createdBy = "createdBy"
        static let homeID = "homeId"
        static let name = "name"
    }
    
    var created: Any?
    var createdBy: String?
    var homeID: String?
    var name: String!
    
    init(_ json: [String: Any]) {
        self.homeID = json[Keys.homeID] as? String
        self.created = json[Keys.created]
        self.createdBy = json[Keys.createdBy] as? String
        self.name = json[Keys.name] as? String ?? ""
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
    
    
}
