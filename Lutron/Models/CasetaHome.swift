//
//  CasetaRooms.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/15/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class CasetaHome: NSObject {
    
    struct Keys {
        static let rooms = "rooms"
        static let name = "name"
        static let time = "time"
    }
    
    var rooms: [CasetaRoom]?
    var name: String!
    var time: Date?

    init(_ json: [String: Any]) {
        self.rooms = json[Keys.rooms] as? [CasetaRoom]
        self.name = json[Keys.name] as? String ?? ""
        self.time = json[Keys.time] as? Date
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
    
    
}
