//
//  CasetaDevice.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/15/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class CasetaDevice: NSObject {
    
    struct Keys {
        static let brightness = "brightness"
        static let created = "created"
        static let createdBy = "createdBy"
        static let deviceID = "deviceId"
        static let homeID = "homeId"
        static let isOn = "isOn"
        static let name = "name"
        static let roomID = "roomId"
        static let iconName = "iconName"
    }
    
    var brightness: Int!
    var created: Date!
    var createdBy: String!
    var deviceID: String!
    var homeID: String!
    var isOn: Int!
    var name: String!
    var roomID: String!
    var iconName: String!
    var deviceDocId: String!
    
    init(_ json: [String: Any], deviceDoc: String) {
        self.brightness = json[Keys.brightness] as? Int ?? 0
        self.created = json[Keys.created] as? Date ?? Date()
        self.createdBy = json[Keys.createdBy] as? String ?? "Dylan"
        self.deviceID = json[Keys.deviceID] as? String ?? ""
        self.homeID = json[Keys.homeID] as? String ?? ""
        self.isOn = json[Keys.isOn] as? Int ?? Constants.DeviceState.OFF
        self.name = json[Keys.name] as? String ?? ""
        self.roomID = json[Keys.roomID] as? String ?? ""
        self.iconName = json[Keys.iconName] as? String ?? "Ceiling Fan Light"
        self.deviceDocId = deviceDoc
    }
    
    convenience init(json: [String: Any], deviceDoc: String) {
        self.init(json, deviceDoc: deviceDoc)
    }
    
    
}
