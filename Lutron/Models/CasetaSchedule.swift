//
//  CasetaSchedule.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/29/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class CasetaSchedule: NSObject {
    
    struct Keys {
        static let created = "created"
        static let createdBy = "createdBy"
        static let daysOfWeek = "daysOfWeek"
        static let devices = "devices"
        static let homeID = "homeId"
        static let isOn = "isOn"
        static let name = "name"
        static let time = "time"
    }
    
    var created: Date!
    var createdBy: String!
    var daysOfWeek: String!
    var devices: [[String: Any]]!
    var homeID: String!
    var isOn: Bool!
    var name: String!
    var time: String!
    
    init(_ json: [String: Any]) {
        self.created = json[Keys.created] as? Date ?? Date()
        self.createdBy = json[Keys.createdBy] as? String ?? "Dylan"
        self.daysOfWeek = json[Keys.daysOfWeek] as? String ?? ""
        self.devices = json[Keys.devices] as? [[String: Any]] ?? [[:]]
        self.homeID = json[Keys.homeID] as? String ?? ""
        self.isOn = json[Keys.isOn] as? Bool ?? false
        self.name = json[Keys.name] as? String ?? ""
        self.time = json[Keys.time] as? String ?? ""
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
    
    
}
