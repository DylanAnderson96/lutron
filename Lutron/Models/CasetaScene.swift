//
//  CasetaScene.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/27/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class CasetaScene: NSObject {
    
    struct Keys {
        static let created = "created"
        static let createdBy = "createdBy"
        static let devices = "devices"
        static let homeID = "homeId"
        static let iconName = "iconName"
        static let name = "name"
        static let isOn = "isOn"
    }
    
    var created: Date!
    var createdBy: String!
    var devices: [[String: Any]]!
    var homeID: String!
    var iconName: String!
    var name: String!
    var isOn: Bool!
    
    init(_ json: [String: Any]) {
        self.created = json[Keys.created] as? Date ?? Date()
        self.createdBy = json[Keys.createdBy] as? String ?? "Dylan"
        self.devices = json[Keys.devices] as? [[String: Any]] ?? [[:]]
        self.homeID = json[Keys.homeID] as? String ?? ""
        self.iconName = json[Keys.iconName] as? String ?? ""
        self.name = json[Keys.name] as? String ?? ""
        self.isOn = json[Keys.isOn] as? Bool ?? false
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
    
    
}
