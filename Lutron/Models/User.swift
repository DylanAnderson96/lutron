//
//  User.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/20/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class User: NSObject {
    
    struct Keys {
        static var company = "Company"
        static var phone = "Phone"
        static var eMail = "E-mail"
        static var password = "Password"
        static var account = "Account"
        static var installerName = "Installer Name"
        static var certifications = "Certifications"
        static var connections = "Connections"
    }
    
    var company: String!
    var phone: String!
    var eMail: String!
    var account: String!
    var password: String!
    var installerName: String!
    var certifications: [String]!
    var connections: [String]!

    init(_ json: [String: Any]) {
        self.company = json[Keys.company] as? String ?? ""
        self.phone = json[Keys.phone] as? String ?? ""
        self.eMail = json[Keys.eMail] as? String ?? ""
        self.account = json[Keys.account] as? String ?? ""
        self.password = json[Keys.password] as? String ?? ""
        self.installerName = json[Keys.installerName] as? String ?? ""
        self.certifications = json[Keys.certifications] as? [String] ?? []
        self.connections = json[Keys.connections] as? [String] ?? []
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
}
