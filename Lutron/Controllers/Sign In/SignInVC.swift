//
//  SignInVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SignInVC: CoreBaseController {
    
    @IBOutlet weak var emailAddressTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var createAccountBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setBackgroundGradient()
        self.signInBtn.layer.cornerRadius = 8
        self.createAccountBtn.backgroundColor = .clear
        self.signInBtn.setTitleColor(.white, for: .normal)
        self.signInBtn.backgroundColor = .lightBlue
        self.createAccountBtn.setTitleColor(.lightBlue, for: .normal)
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismissDetail(transitionDirection: .fromLeft, isAnimated: false)
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        //Determine who is signing in and then launch the specific view based off that
        guard let email = emailAddressTF.text else { return }
        guard let password = passwordTF.text else { return }
        
        if email.isEmpty || password.isEmpty {
            self.displayAlert(title: "Whoops", message: "Please enter an email or password.")
        } else {
            if email.contains("RA2Select") {
                self.launchRA2SelectSystem()
            } else if email.contains("HomeWorks") {
                self.launchHomeWorksSystem()
            } else {
                self.launchListOfHomes()
            }
        }
        
    }
    
    @IBAction func createAccountpressed(_ sender: Any) {
        self.displayAlertToOpenSettings(title: "Create Account", message: "This would send the user to create a new account with myLightKit.")
    }
}
