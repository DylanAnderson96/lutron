//
//  CasetaDevicesTableViewCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/13/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class CasetaScenesTableViewCell: UITableViewCell {
    
    let roomImage: UIImageView = {
        let objectInRoomBtn = UIImageView()
        objectInRoomBtn.translatesAutoresizingMaskIntoConstraints = false
        objectInRoomBtn.isUserInteractionEnabled = true
        objectInRoomBtn.layer.cornerRadius = 30
        objectInRoomBtn.layer.masksToBounds = true
        objectInRoomBtn.clipsToBounds = true
        objectInRoomBtn.contentMode = .scaleAspectFit
//        objectInRoomBtn.backgroundColor = .lightGray
        return objectInRoomBtn
    }()
    
    let editImageBtn: UIButton = {
        let editImageBtn = UIButton()
        editImageBtn.translatesAutoresizingMaskIntoConstraints = false
        editImageBtn.isUserInteractionEnabled = true
//        editImageBtn.layer.cornerRadius = 30
        editImageBtn.layer.masksToBounds = true
        editImageBtn.clipsToBounds = true
        editImageBtn.contentMode = .scaleToFill
        editImageBtn.backgroundColor = .clear
        editImageBtn.tintColor = .darkBlue
        return editImageBtn
    }()
    
    let roomNameLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = .darkBlue
        label.isUserInteractionEnabled = true
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let widthOfObject: CGFloat = 60

        addSubview(roomImage)
        addSubview(editImageBtn)
        addSubview(roomNameLbl)
        
        roomImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        roomImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        roomImage.widthAnchor.constraint(equalToConstant: widthOfObject).isActive = true
        roomImage.heightAnchor.constraint(equalToConstant: widthOfObject).isActive = true

        let editImage = UIImage(named: "editPencil")
        editImageBtn.setImage(editImage, for: .normal)
        editImageBtn.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        editImageBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        editImageBtn.widthAnchor.constraint(equalToConstant: widthOfObject/2).isActive = true
        editImageBtn.heightAnchor.constraint(equalToConstant: widthOfObject/2).isActive = true
        
        roomNameLbl.leftAnchor.constraint(equalTo: self.roomImage.rightAnchor, constant: 10).isActive = true
        roomNameLbl.centerYAnchor.constraint(equalTo: self.roomImage.centerYAnchor, constant: 0).isActive = true
        roomNameLbl.rightAnchor.constraint(equalTo: self.editImageBtn.leftAnchor, constant: -10).isActive = true
        roomNameLbl.heightAnchor.constraint(equalToConstant: widthOfObject).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

