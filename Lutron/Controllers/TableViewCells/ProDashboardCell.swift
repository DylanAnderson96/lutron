//
//  ProDashboardCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/16/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ProDashboardCell: UITableViewCell {
        
    @IBOutlet weak var headerLbl: UIButton!
    @IBOutlet weak var descriptionLbl: UIButton!
    @IBOutlet weak var cellSelectedBtn: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
            
    }
    
}

