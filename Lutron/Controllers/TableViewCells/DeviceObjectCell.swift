//
//  ObjectCollectionViewCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/14/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ObjectCollectionViewCell: UICollectionViewCell {
    
    let imageView: UIButton = {
        let roomObjectImage = UIButton()
        roomObjectImage.translatesAutoresizingMaskIntoConstraints = false
        roomObjectImage.isUserInteractionEnabled = true
        roomObjectImage.layer.cornerRadius = 35
        roomObjectImage.layer.masksToBounds = true
        roomObjectImage.clipsToBounds = true
        roomObjectImage.contentMode = .scaleAspectFit
        return roomObjectImage
    }()
    
    let objectName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        addSubview(objectName)
        
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        objectName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        objectName.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 8).isActive = true
        objectName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        objectName.heightAnchor.constraint(equalToConstant: 40).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
