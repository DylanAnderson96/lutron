//
//  PickDevicesForScenesCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/24/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class PickDevicesForSceneCell: UITableViewCell {
    @IBOutlet weak var isSelectedDevice: UIButton!
    @IBOutlet weak var extendCellBtn: UIButton!
    @IBOutlet weak var nameOfLight: UILabel!
    @IBOutlet weak var deviceStateLbl: UILabel!
    @IBOutlet weak var viewForDevice: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var deviceOn: UIButton!
    @IBOutlet weak var deviceDimemrUp: UIButton!
    @IBOutlet weak var deviceDimmerDown: UIButton! {
           didSet {
               deviceDimmerDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
           }
       }
    @IBOutlet weak var deviceOff: UIButton!
}
