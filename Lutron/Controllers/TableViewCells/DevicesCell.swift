//
//  CasetaDevicesTableViewCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/13/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class CasetaDevicesTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
        
    var numberOfCollectionViewCells: Int = 0
    var cellID = "deviceCellID"
    var collectionView: UICollectionView!
    var roomNameObjects: [String] = []
    var roomImageObjects: [String] = []
    var devices = [CasetaDevice]()
    var deviceState: [Bool] = []
    var deviceId: String = ""
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfCollectionViewCells
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! ObjectCollectionViewCell
        cell.objectName.text = roomNameObjects[indexPath.section]
        let deviceImageName = roomImageObjects[indexPath.section]
        cell.isUserInteractionEnabled = true
        let state = deviceState[indexPath.section]
        var imageName: String = ""
        if deviceImageName == "Other" {
            imageName = "Ceiling Lights"
        } else if deviceImageName == "doors" {
            imageName = "Cabinet Lights"
        } else if deviceImageName == "pendants" {
            imageName = "Pendants"
        } else if deviceImageName == "chandelier" {
            imageName = "Chandelier"
        } else if deviceImageName == "ceilingLights" {
            imageName = "Ceiling Lights"
        } else if deviceImageName == "lightbulb" {
            imageName = "theLightbulb"
        } else {
            imageName = deviceImageName
        }

        let deviceImage = UIImage(named: imageName)?.imageWithInsets(insets: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20))?.withRenderingMode(.alwaysOriginal)

        if state {
            cell.imageView.setImage(deviceImage, for: .normal)
            cell.imageView.backgroundColor = .lightBlue
            cell.imageView.tintColor = .white
        } else {
            cell.imageView.setImage(deviceImage, for: .normal)
            cell.imageView.backgroundColor = .rgba(236,240,241 ,1)
            cell.imageView.tintColor = .darkGray
        }
        
        cell.imageView.tag = indexPath.section
        cell.imageView.addTarget(self, action: #selector(roomObjectTapped(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func roomObjectTapped(sender: UIButton) {
        self.changeDeviceState(device: devices[sender.tag])
    }
    
    func changeDeviceState(device: CasetaDevice) {
        if let parentView = parentViewController as? CasetaSystemView {
            let dimness = Float(device.brightness) * Float(0.01)
            parentView.editDeviceView.alpha = 0.0
            parentView.editDeviceView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            UIView.animate(withDuration: 0.3) {
                parentView.editDeviceView.alpha = 1.0
                parentView.editDeviceView.isHidden = false
                parentView.editDeviceView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                if device.isOn == Constants.DeviceState.ON {
                    parentView.editViewStatusOfLight.text = "\(String(device.brightness)) %"
                } else {
                    parentView.editViewStatusOfLight.text = "\(String(device.brightness)) %"
                }
                parentView.nameOfLightEditView.text = device.name
                parentView.horizontalSlider.setValue(dimness, animated: false)
            }
            self.deviceId = device.deviceID
            parentView.currentDeviceBeingEditted = device
        }
        
    }
    
    func setUpCollectionViewCell() {
        self.deviceState.removeAll()
        for device in self.devices {
            if device.brightness > 0 {
                self.deviceState.append(true)
            } else {
                self.deviceState.append(false)
            }
        }
    }
    
    let roomNameLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 19)
        label.textColor = UIColor.label
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = .lightBlue
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
                
        addSubview(roomNameLbl)

        roomNameLbl.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        roomNameLbl.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        roomNameLbl.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        roomNameLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        roomNameLbl.backgroundColor = .white
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 120, height: 120)

        collectionView = UICollectionView(frame: CGRect(x: 20, y: 0, width: self.frame.width, height: self.frame.height), collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ObjectCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = true
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
        
        collectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        collectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        collectionView.topAnchor.constraint(equalTo: self.roomNameLbl.bottomAnchor, constant: 10).isActive = true
        //cell size is 190 - roomTop,roomheight,spaceToRoom (10+30+10) = 140
        collectionView.heightAnchor.constraint(equalToConstant: 140).isActive = true
                
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

