//
//  AddDeviceTableCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/22/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class AddDeviceTableCell: UITableViewCell {
    
    @IBOutlet weak var newDeviceImg: UIImageView!
    @IBOutlet weak var header: UIButton!
    @IBOutlet weak var cellSelectedBtn: UIButton!
    @IBOutlet weak var selectedCellTick: UIImageView!
    
}

