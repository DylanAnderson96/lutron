//
//  SelectRoomTableCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/22/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SelectRoomTableCell: UITableViewCell {
    @IBOutlet weak var roomNameBtnText: UIButton!
    @IBOutlet weak var cellSelectedBtn: UIButton!
    @IBOutlet weak var selectCellTick: UIImageView!
    
}
