//
//  ListOfHomesCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/15/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ListOfHomesCell: UITableViewCell {
    
    let homeName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
        addSubview(homeName)
        
        homeName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        homeName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        homeName.widthAnchor.constraint(equalToConstant: self.frame.width - 20).isActive = true
        homeName.heightAnchor.constraint(equalToConstant: self.frame.height).isActive = true

    }
    
}
