//
//  DaysOfTheWeekCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/28/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class DaysOfTheWeekTableViewCell: UITableViewCell {
    
    @IBOutlet weak var daysOfTheWeekBtn: UIButton!
    @IBOutlet weak var isSelectedImage: UIImageView!
}

