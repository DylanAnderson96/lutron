//
//  CasetaDevicesTableViewCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/13/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class CasetaScheduleTableViewCell: UITableViewCell {
    
    let scheduleName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.label
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .lightGray
        label.textAlignment = .center
        label.numberOfLines = 4
        label.adjustsFontSizeToFitWidth = true
        label.layer.cornerRadius = 30
        label.layer.masksToBounds = true
        return label
    }()
    
    let editImageBtn: UIButton = {
        let editImageBtn = UIButton()
        editImageBtn.translatesAutoresizingMaskIntoConstraints = false
        editImageBtn.isUserInteractionEnabled = true
        editImageBtn.layer.cornerRadius = 20
        editImageBtn.layer.masksToBounds = true
        editImageBtn.clipsToBounds = true
        editImageBtn.contentMode = .scaleToFill
        editImageBtn.backgroundColor = .clear
        editImageBtn.tintColor = .lightBlue
        return editImageBtn
    }()
    
    let scheduleDays: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor.label
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textAlignment = .left
        label.textColor = .lightBlue
        label.numberOfLines = 2
        return label
    }()
    
    let scheduleDaysOfTheWeek: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textAlignment = .left
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let widthOfObject: CGFloat = 60

        addSubview(scheduleName)
        addSubview(editImageBtn)
        addSubview(scheduleDays)
        addSubview(scheduleDaysOfTheWeek)
        
        scheduleName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        scheduleName.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        scheduleName.widthAnchor.constraint(equalToConstant: widthOfObject).isActive = true
        scheduleName.heightAnchor.constraint(equalToConstant: widthOfObject).isActive = true

        let editImage = UIImage(named: "editPencil")
        editImageBtn.setImage(editImage, for: .normal)
        editImageBtn.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        editImageBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        editImageBtn.widthAnchor.constraint(equalToConstant: widthOfObject/2).isActive = true
        editImageBtn.heightAnchor.constraint(equalToConstant: widthOfObject/2).isActive = true
        
        scheduleDays.leftAnchor.constraint(equalTo: self.scheduleName.rightAnchor, constant: 10).isActive = true
        scheduleDays.centerYAnchor.constraint(equalTo: self.scheduleName.centerYAnchor, constant: -15).isActive = true
        scheduleDays.rightAnchor.constraint(equalTo: self.editImageBtn.leftAnchor, constant: -10).isActive = true
        scheduleDays.heightAnchor.constraint(equalToConstant: widthOfObject/2).isActive = true

        scheduleDaysOfTheWeek.leftAnchor.constraint(equalTo: self.scheduleName.rightAnchor, constant: 10).isActive = true
        scheduleDaysOfTheWeek.topAnchor.constraint(equalTo: self.scheduleDays.bottomAnchor, constant: 10).isActive = true
        scheduleDaysOfTheWeek.rightAnchor.constraint(equalTo: self.editImageBtn.leftAnchor, constant: -10).isActive = true
        scheduleDaysOfTheWeek.heightAnchor.constraint(equalToConstant: widthOfObject/2 - 20).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

