//
//  SettingsTableViewCell.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/21/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var selectedCellBtn: UIButton!
    @IBOutlet weak var settingsHeaderBtn: UIButton!
    
}


