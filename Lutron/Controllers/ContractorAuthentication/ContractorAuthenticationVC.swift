//
//  ContractorAuthenticationVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/16/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ContractorAuthenticationVC: CoreBaseController, UITextFieldDelegate {
    
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var installedNamelbl: UILabel!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var companyTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var accountTF: UITextField!
    @IBOutlet weak var installedNameTF: UITextField!
    @IBOutlet weak var infoBtnAcc: UIButton!
    @IBOutlet weak var infomationView: UIView!
    @IBOutlet weak var signOutOfProDashboardBtn: UIButton!
    
    //Embeded in the View
    @IBOutlet weak var earnRewardsLbl: UILabel!
    @IBOutlet weak var includeLutronAccountLbl: UILabel!
    @IBOutlet weak var helpFindingLutronAccountNoBtn: UIButton!
    @IBOutlet weak var signUpForLutronPROBtn: UIButton!
    
    var hasShowPossibilityToEarnPoints: Bool = false
    var canEditInfo: Bool = false
    var contractor: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addContractorDetails()
        self.setUpView()
    }
    
    func setUpView() {
        self.navigationController?.navigationBar.isHidden = false

        self.infomationView.backgroundColor = .systemBackground
        self.infomationView.layer.cornerRadius = 5
        self.infomationView.applyAShadow()
        self.infomationView.layer.borderWidth = 1
        self.infomationView.layer.borderColor = UIColor.label.cgColor

        let viewTappedGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.view.addGestureRecognizer(viewTappedGesture)
        
        self.nextBtn.isEnabled = false
        self.nextBtn.tintColor = .darkGray
        
        self.companyTF.backgroundColor = .clear
        self.phoneTF.backgroundColor = .clear
        self.emailTF.backgroundColor = .clear
        self.accountTF.backgroundColor = .clear
        self.installedNameTF.backgroundColor = .clear
        
        //Within the pop up view next to Lutron Account # Textfield info button
        self.earnRewardsLbl.adjustsFontSizeToFitWidth = true
        self.includeLutronAccountLbl.adjustsFontSizeToFitWidth = true
        self.earnRewardsLbl.numberOfLines = 1
        self.includeLutronAccountLbl.numberOfLines = 2
        self.helpFindingLutronAccountNoBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.helpFindingLutronAccountNoBtn.titleLabel?.numberOfLines = 2
        self.signUpForLutronPROBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.signUpForLutronPROBtn.titleLabel?.numberOfLines = 2

        //Delegates
        self.companyTF.delegate = self
        self.phoneTF.delegate = self
        self.emailTF.delegate = self
        self.accountTF.delegate = self
        self.installedNameTF.delegate = self
        
        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }

        if companyName.isEmpty && phoneNumber.isEmpty && email.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
        
        if canEditInfo {
            self.signOutOfProDashboardBtn.isHidden = false
            self.nextBtn.title = "Save"
        } else {
            self.signOutOfProDashboardBtn.isHidden = true
            self.nextBtn.title = "Next"
        }
        
    }
    
    @objc func viewTapped() {
        if !self.infomationView.isHidden {
            self.infomationView.isHidden = true
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }
        
        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    func addContractorDetails() {
        if canEditInfo {
            self.companyTF.isEnabled = true
            self.phoneTF.isEnabled = true
            self.emailTF.isEnabled = true
            self.signOutOfProDashboardBtn.isHidden = true
        } else {
            self.companyTF.isEnabled = false
            self.phoneTF.isEnabled = false
            self.emailTF.isEnabled = false
            self.signOutOfProDashboardBtn.isHidden = false
        }
        guard let authenticatedContractor = contractor else { return }
        self.companyTF.text = authenticatedContractor.company
        self.phoneTF.text = authenticatedContractor.phone
        self.emailTF.text = authenticatedContractor.eMail
        self.accountTF.text = authenticatedContractor.account
        self.installedNameTF.text = authenticatedContractor.installerName
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let companyName = companyTF.text else { return true }
        guard let phoneNumber = phoneTF.text else { return true }
        guard let email = emailTF.text else { return true }
        
        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
        return true
    }

    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signOutOfProDashboardPressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Sign Out of Pro Dashboard", message: "Are you sure you would like to do this?", alertType: .actionSheet)
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }
        guard let accountNum = accountTF.text else { return }
        guard let authenticatedContractor = contractor else { return }
        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
            self.displayAlert(title: "Whoops", message: "Please Enter a Company Name, Phone No. and Email to continue.")
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
            
            if hasShowPossibilityToEarnPoints || !accountNum.isEmpty {
                self.infomationView.isHidden = true
                if canEditInfo {
                    if nextBtn.title == "Save" {
                        self.displayAlertWithClosure(title: "Success", message: "Your profile has been updated.", alertType: .alert)
                    } else {
                        //Using account number count being auth of user
                        if accountNum.count >= 6 {
                            print("account number success")
                            self.goToProDashboardWithAuthenticatedContractor(contractor: authenticatedContractor)
                        } else {
                            print("account number error")
                            self.displayAlert(title: "Whoops", message: "Please enter a valid account number to continue or go to next screen")
                        }
                    }
                } else {
                    self.goToProDashboardWithAuthenticatedContractor(contractor: authenticatedContractor)
                }
            } else {
                self.infomationView.isHidden = false
                hasShowPossibilityToEarnPoints = true
            }
        }
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.navigationController?.popViewController(animated: true)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.dismiss(animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func fundLutronPROAccountNoPressed(_ sender: Any) {
        self.displayAlert(title: "LightKit Account #", message: "This would go to a FAQ view to show the contractor how to find their account # or sign up if they don't have an account")
    }
    
    @IBAction func signUpLutronPROBtnPressed(_ sender: Any) {
        self.displayAlert(title: "Sign Up", message: "In production this would send the contractor to a view to sign Up for LightKit PRO.")
    }
    
    @IBAction func infoForAccountPressed(_ sender: Any) {
        if self.infomationView.isHidden {
            self.infomationView.isHidden = false
        } else {
            self.infomationView.isHidden = true
        }
    }
    
}
