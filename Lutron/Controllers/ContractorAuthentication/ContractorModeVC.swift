//
//  ContractorModeVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/21/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ContractorModeVC: CoreBaseController, UITextFieldDelegate {

    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    
    @IBOutlet weak var topLblPrivacyPolicy: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var phonelbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var installerNameLbl: UILabel!
    
    @IBOutlet weak var companyTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var accountTF: UITextField!
    @IBOutlet weak var installerNameTF: UITextField!

    @IBOutlet weak var accountInfoBtn: UIButton!
    
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var earnRewardsLbl: UILabel!
    @IBOutlet weak var includeLutroProLbl: UILabel!
    @IBOutlet weak var helpFIndingLutronProBtn: UIButton!
    @IBOutlet weak var signUpLutronPro: UIButton!
    
    var contractors: [User] = []
    var users: [[String: Any]] = [[:]]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        
        self.users = Contractors.contractors
        for user in users {
            contractors.append(User(user))
        }

    }
    
    func setUpView() {
        self.informationView.isHidden = false
        self.informationView.backgroundColor = .systemBackground
        self.informationView.layer.cornerRadius = 5
        self.informationView.applyAShadow()
        self.informationView.layer.borderWidth = 1
        self.informationView.layer.borderColor = UIColor.label.cgColor
        
        let viewTappedGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        self.view.addGestureRecognizer(viewTappedGesture)
        
        //Within the pop up view next to Lutron Account # Textfield info button
        self.earnRewardsLbl.adjustsFontSizeToFitWidth = true
        self.includeLutroProLbl.adjustsFontSizeToFitWidth = true
        self.earnRewardsLbl.numberOfLines = 1
        self.includeLutroProLbl.numberOfLines = 2
        self.helpFIndingLutronProBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.helpFIndingLutronProBtn.titleLabel?.numberOfLines = 2
        self.signUpLutronPro.titleLabel?.adjustsFontSizeToFitWidth = true
        self.signUpLutronPro.titleLabel?.numberOfLines = 2
        
        let infoButton = UIImage(named: "info")?.imageWithInsets(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        self.accountInfoBtn.setImage(infoButton, for: .normal)
        
        //Delegates
        self.companyTF.delegate = self
        self.phoneTF.delegate = self
        self.emailTF.delegate = self
        self.accountTF.delegate = self
        self.installerNameTF.delegate = self
        
        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }
        guard let account = accountTF.text else { return }
        guard let installerName = installerNameTF.text else { return }

        if companyName.isEmpty && phoneNumber.isEmpty && email.isEmpty && account.isEmpty && installerName.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    @objc func viewTapped() {
        if !self.informationView.isHidden {
            self.informationView.isHidden = true
        }
    }
    
    //When user selects different part that is not current textfield this function gets called
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }
        guard let account = accountTF.text else { return }
        guard let installerName = installerNameTF.text else { return }

        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty || account.isEmpty || installerName.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    //This function gets called while the user is typing but is 1 letter behind the user each time
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let companyName = companyTF.text else { return true }
        guard let phoneNumber = phoneTF.text else { return true }
        guard let email = emailTF.text else { return true }
        guard let account = accountTF.text else { return true }
        guard let installerName = installerNameTF.text else { return true }

        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty || account.isEmpty || installerName.isEmpty {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
        return true
    }
    

    @IBAction func accountInfoBtnPressed(_ sender: Any) {
        if self.informationView.isHidden {
            self.informationView.isHidden = false
        } else {
            self.informationView.isHidden = true
        }
    }
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true )
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {

        guard let companyName = companyTF.text else { return }
        guard let phoneNumber = phoneTF.text else { return }
        guard let email = emailTF.text else { return }
        guard let account = accountTF.text else { return }
        guard let installerName = installerNameTF.text else { return }
        
        if companyName.isEmpty || phoneNumber.isEmpty || email.isEmpty || account.isEmpty || installerName.isEmpty {
            self.displayAlert(title: "Whoops", message: "Please make sure all fields are valid and try again.")
        } else {
            let formattedNumber = self.convertPhoneNumberToCorrectFormat(phone: phoneNumber)
            if self.isUserAuthenticated(companyName: companyName, number: formattedNumber, email: email, accountNum: account, installerName: installerName) {
                guard let authenticatedContractor = self.getCurrentContractor(email: email) else { return }
                self.goToProDashboardWithAuthenticatedContractor(contractor: authenticatedContractor)
            } else {
                self.displayAlert(title: "Whoops", message: "Invalid information. Please try again.")
            }
        }
    }
    
    func convertPhoneNumberToCorrectFormat(phone: String) -> String {
        let cleanPhoneNumber = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @IBAction func helpFindingLutronProPressed(_ sender: Any) {
        self.displayAlert(title: "LightKit Account #", message: "This would go to a FAQ view to show the contractor how to find their account # or sign up if they don't have an account")
    }
    
    @IBAction func signUpLutronProPressed(_ sender: Any) {
        self.displayAlert(title: "Sign Up", message: "In production this would send the contractor to a view to sign Up for LightKit PRO.")
    }
    
    func getCurrentContractor(email: String) -> User? {
        for contractor in contractors {
            if email == contractor.eMail {
                return contractor
            }
        }
        return nil
    }
    
    func isUserAuthenticated(companyName: String, number: String, email: String, accountNum: String, installerName: String) -> Bool {
        for contractor in contractors {
            if email == contractor.eMail {
                if number == contractor.phone && companyName == contractor.company && accountNum == contractor.account && installerName == contractor.installerName {
                    return true
                } else {
                    return false
                }
            }
        }
        return false
    }
}
