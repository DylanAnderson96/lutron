//
//  ViewController.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class InitialVC: CoreBaseController {

    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var proDashboardBtn: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var infoCircleImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoImageTapped()
        self.getStartedButton.layer.cornerRadius = 8
        self.signInBtn.layer.cornerRadius = 8
        self.proDashboardBtn.layer.cornerRadius = 8
        self.getStartedButton.setTitleColor(.lightBlue, for: .normal)
        self.signInBtn.setTitleColor(.lightBlue, for: .normal)
        self.proDashboardBtn.setTitleColor(.lightBlue, for: .normal)
        self.seperatorView.backgroundColor = .lightGray
        self.addLightKitImage()
        self.getStartedButton.backgroundColor = .clear
        self.getStartedButton.layer.borderColor = UIColor.clear.cgColor
        self.signInBtn.backgroundColor = .clear
        self.signInBtn.layer.borderColor = UIColor.clear.cgColor
        self.proDashboardBtn.backgroundColor = .clear
        self.proDashboardBtn.layer.borderColor = UIColor.clear.cgColor
        
    }
    
    @IBAction func getStartedPressed(_ sender: Any) {
        self.displayAlert(title: "Installation Process", message: "This would guide the user through the installation process to connect their device to the hardware.")
    }
    
    func addLightKitImage() {
        let label = UILabel(frame: CGRect(x: 20, y: view.frame.height/4, width: view.frame.width - 40, height: view.frame.height/5.5))
        label.backgroundColor = .lightBlue
        label.text = "LightKit"
        label.font = UIFont.systemFont(ofSize: 72)
        label.textAlignment = .center
        label.textColor = .white
        view.addSubview(label)
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        let signInVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SignInVC) as! SignInVC
        self.presentDetail(signInVC, transitionType: .fromRight)
    }
    
    @IBAction func proDashboardPressed(_ sender: Any) {
        let proDashboardVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ProDashboardVC) as! ProDashboardVC
        self.presentDetail(proDashboardVC, transitionType: .fromRight)
    }
    
    @IBAction func unwindFromLogOut(sender: UIStoryboardSegue) {
        print("im back")
    }
    
    func infoImageTapped() {
        self.infoCircleImg.isUserInteractionEnabled = true
        let infoButtonTapGesture = UITapGestureRecognizer(target: self, action: #selector(infoImageBtnTapped))
        self.infoCircleImg.addGestureRecognizer(infoButtonTapGesture)
    }
    
    @objc func infoImageBtnTapped() {
        self.displayAlert(title: "Pro Dashboard", message: "Allows you to set up systems WITHOUT having to log out of Personal Home if you are logged into MyLightKit.")
    }
    
}

