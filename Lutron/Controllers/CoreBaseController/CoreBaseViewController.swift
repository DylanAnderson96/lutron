//
//  CoreBaseViewController.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import UserNotifications
import AVFoundation
import Photos
import CoreMotion
import AudioToolbox
import CoreData

class CoreBaseController: UIViewController, UINavigationControllerDelegate, NVActivityIndicatorViewable, UINavigationBarDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            print("motion sjake")
        }
    }
    
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func validatemail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
            
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
            
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
            
       return normalizedImage
    }
    
    //MARK: - Set navigation bar visual properties
    func setUpNavigationBar() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func addBackButton() {
        let backButtonTapped = UITapGestureRecognizer(target: self, action: #selector(dismissScreen))
        let backButton = UIImageView(frame: CGRect(x: 25, y: 25, width: 30, height: 30))
        backButton.backgroundColor = .clear
        backButton.isUserInteractionEnabled = true
        backButton.contentMode = .scaleAspectFit
        backButton.image = UIImage(named: "leftArrow.png")?.withRenderingMode(.alwaysTemplate)
        backButton.tintColor = .white
        backButton.addGestureRecognizer(backButtonTapped)
        
        view.addSubview(backButton)
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayToastPopUp(controller: UIViewController, message: String, time: Double) {
        let attributedString = NSAttributedString(string: message, attributes: [
            NSAttributedString.Key.foregroundColor : UIColor.white
            ])
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        alert.view.alpha = 0.6
        alert.view.tintColor = UIColor.white
        alert.view.layer.cornerRadius = 15
        alert.setValue(attributedString, forKey: "attributedMessage")
        
        controller.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
            alert.dismiss(animated: true)
        }
    }
    
    func showAlert(message: String, title: String = "Alert", preferredStyle: UIAlertController.Style = .alert){
           let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
           let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
           alert.addAction(defaultAction)
           self.present(alert, animated: true, completion: nil)
       }
    
    func setBackgroundGradient() {
        self.view.applyGradientToView(colours: [.link, .lightBlue], fromX: 0, toX: 1, fromY: 0, toY: 1)
    }
    
    //MARK: - Change status bar background Color
    func statusBarColor(color: UIColor) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
    
    func goToProDashboardWithAuthenticatedContractor(contractor: User) {
        let proDashboardVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ProDashboardVC) as! ProDashboardVC
        proDashboardVC.modalPresentationStyle = .fullScreen
        proDashboardVC.isDefaultScreenShowing = false
        proDashboardVC.isContractorAuthenticated = true
        proDashboardVC.contractor = contractor
        self.navigationController?.pushViewController(proDashboardVC, animated: true)
    }
    
    func launchContractorAuth(isEditable: Bool, contractor: User) {
        let contractorAuthVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ContractorAuthenticationVC) as! ContractorAuthenticationVC
        contractorAuthVC.modalPresentationStyle = .fullScreen
        contractorAuthVC.canEditInfo = isEditable
        contractorAuthVC.contractor = contractor
        self.navigationController?.pushViewController(contractorAuthVC, animated: true)
    }
    
    func launchListOfHomes() {
        let listOfHomesVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ListOfHomesVC) as! ListOfHomesVC
        listOfHomesVC.modalPresentationStyle = .fullScreen
        self.present(listOfHomesVC, animated: true, completion: nil)
    }
    
    func launchCasetaSystem() {
        let casetaSystemVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.CasetaSystemView) as! CasetaSystemView
        casetaSystemVC.modalPresentationStyle = .fullScreen
        self.present(casetaSystemVC, animated: true, completion: nil)
    }
    
    func launchRA2SelectSystem() {
        let RA2SelectVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.RA2SelectSystemView) as! RA2SelectSystemView
        RA2SelectVC.modalPresentationStyle = .fullScreen
        self.present(RA2SelectVC, animated: true, completion: nil)
    }
    
    func launchHomeWorksSystem() {
        let homeworksSystemVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.HomeWorksSystemView) as! HomeWorksSystemView
        homeworksSystemVC.modalPresentationStyle = .fullScreen
        self.present(homeworksSystemVC, animated: true, completion: nil)
    }
    
    //MARK: - NVActivity Indicator
    func removeTheLoadingIndicator(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    func showTheLoadingIndicator(){
        startAnimating(CGSize(width: 50, height: 50), message: nil, type: NVActivityIndicatorType.ballScaleRippleMultiple, color: UIColor.white , fadeInAnimation: nil)
    }
    
}
