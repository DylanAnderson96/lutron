//
//  SettingsVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/21/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SettingsVC: CoreBaseController {
    
    var settingsArray: [String] = [
        "Add Device", "", "Customize App", "Edit Home",  "", "Widget", "Apple Watch", "", "Arriving/Leaving Home", "HomeKit & Siri", "", "Advanced", "Help", "", "Account",
    ]
    
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var homeId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = .rgba(242, 242, 242, 1)
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        self.tableView.tableHeaderView?.backgroundColor = .clear

        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: tableView.frame.maxY, width: tableView.frame.width, height: 1))
        self.tableView.tableFooterView?.backgroundColor = .clear

    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        self.dismissDetail(transitionDirection: .fromTop, isAnimated: true)
    }
    
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! SettingsTableViewCell
        let cellOptionName = settingsArray[indexPath.row]
        cell.settingsHeaderBtn.setTitle(cellOptionName, for: .normal)
        cell.settingsHeaderBtn.titleLabel?.numberOfLines = 1
        cell.settingsHeaderBtn.titleLabel?.adjustsFontSizeToFitWidth = false
        cell.settingsHeaderBtn.tag = indexPath.row
        cell.settingsHeaderBtn.tag = indexPath.row
        cell.settingsHeaderBtn.addTarget(self, action: #selector(selectedCell(sender:)), for: .touchUpInside)
        cell.selectedCellBtn.addTarget(self, action: #selector(selectedCell(sender:)), for: .touchUpInside)
        
        if cellOptionName.isEmpty {
            cell.selectedCellBtn.isHidden = true
            cell.settingsHeaderBtn.isHidden = true
            cell.iconImg.isHidden = true
        } else {
            if cellOptionName == "Arriving/Leaving Home" {
                cell.iconImg.image = UIImage(named: "ArrivingLeaving Home")
            } else {
                cell.iconImg.image = UIImage(named: cellOptionName)
            }
        }
        cell.isUserInteractionEnabled = true
        return cell
    }
    
    @objc func selectedCell(sender: UIButton) {
        if !settingsArray[sender.tag].isEmpty {
            if sender.tag == 0 {
                self.addNewDevice()
            } else if sender.tag == 14 {
                self.displayAlertWithClosure(title: "Are you sure", message: "Please confirm your logout", alertType: .actionSheet)
            } else {
                self.displayAlert(title: "Action for \(settingsArray[sender.tag])", message: "This would carry out the action to the required view for \(settingsArray[sender.tag]).")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !settingsArray[indexPath.row].isEmpty {
            if indexPath.row == 0 {
                self.addNewDevice()
            } else if indexPath.row == 14 {
                self.displayAlertWithClosure(title: "Are you sure", message: "Please confirm your logout", alertType: .actionSheet)
            } else {
                self.displayAlert(title: "Action for \(settingsArray[indexPath.row])", message: "This would carry out the action to the required view for \(settingsArray[indexPath.row])")
            }
        }
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "segueToInitialVC", sender: nil)
                    UserDefaults.standard.removeObject(forKey: "homeId")
                    UserDefaults.standard.removeObject(forKey: "homeName")
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
    
    func addNewDevice() {
        let pairDeviceVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.PairDevice) as! PairDevice
        pairDeviceVC.homeId = homeId
        let navController = UINavigationController(rootViewController: pairDeviceVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellOptionName = settingsArray[indexPath.row]
        if cellOptionName.isEmpty {
            return 20
        } else {
            return 40
        }
    }
    
}
