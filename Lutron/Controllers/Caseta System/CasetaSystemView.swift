//
//  CasetaSystemView.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class CasetaSystemView: CoreBaseController {
    
    @IBOutlet weak var settingsBtn: UIImageView!
    @IBOutlet weak var lightBultCount: UILabel!
    @IBOutlet weak var devicesScrenesSchedules: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainingTableViewAndSegmentControl: UIView!
    @IBOutlet weak var viewUnderDevices: UIView!
    @IBOutlet weak var viewUnderScenes: UIView!
    @IBOutlet weak var viewUnderSchedules: UIView!
    @IBOutlet weak var lightbulbImage: UIImageView!
    
    //Edit Device View
    @IBOutlet weak var editDeviceView: UIView!
    @IBOutlet weak var horizontalSlider: UISlider!
    @IBOutlet weak var nameOfLightEditView: UILabel!
    @IBOutlet weak var editViewStatusOfLight: UILabel!
    @IBOutlet weak var editViewOnBtn: UIButton!
    @IBOutlet weak var editViewDimmerIncrementUpBtn: UIButton!
    @IBOutlet weak var editDimmerDownBtn: UIButton! {
        didSet {
            editDimmerDownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
    }
    @IBOutlet weak var editViewOffBtn: UIButton!
    @IBOutlet weak var editDeviceBtn: UIButton!
    
    var currentIndex = 0
    let db = Firestore.firestore()
    @IBOutlet weak var nameOfHomeLabel: UILabel!
    var homeName: String = ""

    var homeId: String = ""
    var roomIds: [String] = []
    var devices = [String: [CasetaDevice]]()
    var devicesInRoom = [CasetaDevice]()
    var devicesCurrentlyOn = [CasetaDevice]()
    var currentDeviceBeingEditted: CasetaDevice!
    
    var casetaScene = [CasetaScene]()
    var casetaSceneCurrentlySelected = [["scene": CasetaScene.self, "isSelected": false]]
    
    var casetaSchedule = [CasetaSchedule]()
    var casetaScheduleCurrentlySelected = [["schedule": CasetaSchedule.self, "isSelected": false]]
    var schedulesTimes: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.setUpVisuals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getFirebaseDevices()
        self.getFirebaseScenes()
        self.getFirebaseScedules()
    }
    
    func registerCells() {
        self.tableView.register(CasetaDevicesTableViewCell.self, forCellReuseIdentifier: "devicesCell")
        self.tableView.register(CasetaScenesTableViewCell.self, forCellReuseIdentifier: "scenesCell")
        self.tableView.register(CasetaScheduleTableViewCell.self, forCellReuseIdentifier: "schedulesCell")
    }
    
    func setUpVisuals() {
        let viewTapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(viewTapGesture)
        
        let settingsImage = UIImage(named: "settings")?.withRenderingMode(.alwaysTemplate)
        settingsBtn.image = settingsImage
        settingsBtn.tintColor = .white
        settingsBtn.isUserInteractionEnabled = true
        let settingsTapGesture = UITapGestureRecognizer(target: self, action: #selector(settingsTapped))
        settingsBtn.addGestureRecognizer(settingsTapGesture)
        
        self.tableView.tableFooterView = UIView()
        self.view.applyGradientToView(colours: [.darkBlue, .lightBlue], fromX: 0, toX: 1, fromY: 0, toY: 1)
        self.viewContainingTableViewAndSegmentControl.clipsToBounds = true
        self.viewContainingTableViewAndSegmentControl.layer.cornerRadius = 10
        self.viewContainingTableViewAndSegmentControl.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.devicesScrenesSchedules.selectedSegmentIndex = Constants.SegmentControlState.devices
        let font = UIFont.boldSystemFont(ofSize: 20)
        self.devicesScrenesSchedules.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        self.currentIndex = Constants.SegmentControlState.devices
        self.devicesScrenesSchedules.tintColor = .white
        self.devicesScrenesSchedules.backgroundColor = .white
        self.devicesScrenesSchedules.selectedSegmentTintColor = .darkBlue
        self.devicesScrenesSchedules.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        self.devicesScrenesSchedules.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkBlue], for: .normal)
        
        self.editDeviceView.applyAShadow()
        self.editDeviceView.layer.cornerRadius = 8
        
        self.editViewOnBtn.layer.cornerRadius = self.editViewOnBtn.frame.width/2
        self.editViewDimmerIncrementUpBtn.layer.cornerRadius = self.editViewDimmerIncrementUpBtn.frame.width/2
        self.editDimmerDownBtn.layer.cornerRadius = self.editDimmerDownBtn.frame.width/2
        self.editViewOffBtn.layer.cornerRadius = self.editViewOffBtn.frame.width/2
        self.editDimmerDownBtn.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.editViewDimmerIncrementUpBtn.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.editViewOffBtn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.nameOfHomeLabel.text = homeName
        
        self.devicesScrenesSchedules.addUnderlineForSelectedSegment()
        self.viewUnderDevices.backgroundColor = .lightBlue
        self.viewUnderScenes.backgroundColor = .clear
        self.viewUnderSchedules.backgroundColor = .clear
    }
    
    @objc func settingsTapped() {
        let settingsVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SettingsVC) as! SettingsVC
        settingsVC.homeId = homeId
        self.presentDetail(settingsVC, transitionType: .fromTop)
    }
    
    @objc func viewTapped() {
        self.editDeviceView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        if !editDeviceView.isHidden {
            UIView.animate(withDuration: 0.3) {
                self.editDeviceView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }
            self.editDeviceView.alpha = 0.0
            self.editDeviceView.isHidden = true
        }
    }
    
    @IBAction func unwindToDevices(sender: UIStoryboardSegue) {
        self.getFirebaseDevices()
    }
    
    @IBAction func unwindToScenes(sender: UIStoryboardSegue) {
        self.currentIndex = 1
        self.devicesScrenesSchedules.selectedSegmentIndex = self.currentIndex
        self.tableView.reloadData()
    }
    
    @IBAction func unwindFromEditScenes(sender: UIStoryboardSegue) {
        self.getFirebaseScenes()
        self.currentIndex = 1
        self.devicesScrenesSchedules.selectedSegmentIndex = self.currentIndex
        self.tableView.reloadData()
    }
    
    @IBAction func unwindFromEditSchedules(sender: UIStoryboardSegue) {
        self.getFirebaseScedules()
        self.currentIndex = 2
        self.devicesScrenesSchedules.selectedSegmentIndex = self.currentIndex
        self.tableView.reloadData()
    }
    
    func addMockDevice() {
        let mockData: [String: Any] = ["brightness": 0, "created": Date(), "createdBy": "Dylan", "deviceId": "deviceId1", "homeId": "3DgvPGizLCNUcLdJYhzk", "isOn": false, "name": "Device Name", "roomId": "Basement"]
        db.collection("devices").document("i").setData(mockData) { (error) in
            if error != nil {
                print("There is an error saving the data to firebase")
            } else {
                print("success")
            }
        }
    }
    
    @IBAction func horizontalSliderDragged(_ sender: UISlider) {
        let brightnessValue = Int(sender.value*100)
        var isOn: Bool = false
        if brightnessValue > 0 {
            isOn = true
        } else {
            isOn = false
        }
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDeviceBeingEditted.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : isOn,
                "brightness" : brightnessValue
            ])
            if isOn {
                self.editViewStatusOfLight.text = "\(brightnessValue) %"
            } else {
                self.editViewStatusOfLight.text = "\(brightnessValue) %"
            }
        }
    }
    
    @IBAction func editDeviceTurnOn(_ sender: Any) {
        let db = Firestore.firestore()
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDeviceBeingEditted.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : true,
                "brightness" : 100
            ])
            self.editViewStatusOfLight.text = "ON"
            UIView.animate(withDuration: 0.3) {
                self.horizontalSlider.setValue(1.0, animated: true)
            }
        }
    }
    
    @IBAction func editLightDimmerUp(_ sender: Any) {
        var brightnessValue = Int(currentDeviceBeingEditted.brightness)
        var incremenetedBrightness: Float = Float(brightnessValue) / Float(100)
        brightnessValue += 1
        incremenetedBrightness += 0.01
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDeviceBeingEditted.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : true,
                "brightness" : brightnessValue
            ])
            self.editViewStatusOfLight.text = "ON"
            UIView.animate(withDuration: 0.3) {
                self.horizontalSlider.setValue(incremenetedBrightness, animated: true)
            }
        }
    }
    
    @IBAction func editLightDimmerDown(_ sender: Any) {
        var brightnessValue = Int(currentDeviceBeingEditted.brightness)
        var incremenetedBrightness: Float = Float(brightnessValue) / Float(100)
        brightnessValue -= 1
        incremenetedBrightness -= 0.01
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDeviceBeingEditted.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "brightness" : brightnessValue
            ])
            UIView.animate(withDuration: 0.3) {
                self.horizontalSlider.setValue(incremenetedBrightness, animated: true)
            }
        }
    }
    
    @IBAction func editLightOffbtn(_ sender: Any) {
        let db = Firestore.firestore()
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDeviceBeingEditted.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : false,
                "brightness" : 0
            ])
            self.editViewStatusOfLight.text = "OFF"
            UIView.animate(withDuration: 0.3) {
                self.horizontalSlider.setValue(0.0, animated: true)
            }
        }
    }
    
    @IBAction func editViewEditDevice(_ sender: Any) {
        let editDeviceView = self.storyboard?.instantiateViewController(identifier: Constants.Segues.EditDeviceVC) as! EditDeviceVC
        editDeviceView.currentDevice = currentDeviceBeingEditted
        let navController = UINavigationController(rootViewController: editDeviceView)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true) {
            self.editDeviceView.isHidden = true
        }
    }
    
    func getFirebaseDevices() {
        let roomsCollection = db.collection(Constants.FirestoreCollections.devices)
        let query = roomsCollection.whereField("homeId", isEqualTo: homeId)
        query.addSnapshotListener({ (snapshot, error) in
            if error != nil {
                print("There was an error getting the data \(String(describing: error?.localizedDescription))")
            }
            self.devices.removeAll()
            self.devicesInRoom.removeAll()
            self.devicesCurrentlyOn.removeAll()
            self.roomIds.removeAll()
            
            let devices = snapshot!.documents
            for device in devices {
                self.devicesInRoom.append(CasetaDevice(device.data(), deviceDoc: device.documentID))
                let roomID: String = device.data()["roomId"] as? String ?? ""
                if self.devices[roomID] == nil{
                    self.devices[roomID] = [CasetaDevice]()
                    self.roomIds.append(roomID)
                }
                self.devices[roomID]?.append(CasetaDevice(device.data(), deviceDoc: device.documentID))
                if device.data()["brightness"] as? Int ?? 0 > 0 {
                    self.devicesCurrentlyOn.append(CasetaDevice(device.data(), deviceDoc: device.documentID))
                }
                if self.devicesCurrentlyOn.isEmpty {
                    self.lightBultCount.text = "All Off"
                } else if devices.count == self.devicesCurrentlyOn.count {
                    self.lightBultCount.text = "All On"
                } else {
                    self.lightBultCount.text = "\(self.devicesCurrentlyOn.count) lights on"
                }
            }
            self.tableView.reloadData()
            if self.devicesCurrentlyOn.count > 0 {
                DispatchQueue.main.async {
                    self.lightbulbImage.image = UIImage(named: "theLightbulb")
                }
            } else {
                DispatchQueue.main.async {
                    self.lightbulbImage.image = UIImage(named: "lightsOut")
                }
            }
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "devicesCell") as! CasetaDevicesTableViewCell
            cell.collectionView.reloadData()
        })
        
    }
    
    func getFirebaseScenes() {
        let scenesCollection = db.collection(Constants.FirestoreCollections.scenes)
        let query = scenesCollection.whereField("homeId", isEqualTo: homeId)
        query.addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("There was an error getting the data from Firebase rooms \(String(describing: error?.localizedDescription))")
            } else {
                self.casetaScene.removeAll()
                self.casetaSceneCurrentlySelected.removeAll()
                
                guard let documents = snapshot?.documents else { return }
                for document in documents {
                    self.casetaSceneCurrentlySelected.append(["isSelected": false, "scene": CasetaScene(document.data())])
                    self.casetaScene.append(CasetaScene(document.data()))
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func getFirebaseScedules() {
        let scenesCollection = db.collection(Constants.FirestoreCollections.schedules)
        let query = scenesCollection.whereField("homeId", isEqualTo: homeId)

        query.addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("There was an error getting the data from Firebase rooms \(String(describing: error?.localizedDescription))")
            } else {
                self.casetaSchedule.removeAll()
                self.casetaScheduleCurrentlySelected.removeAll()
                self.schedulesTimes.removeAll()
                
                guard let documents = snapshot?.documents else { return }
                for document in documents {
                    self.casetaScheduleCurrentlySelected.append(["isSelected": false, "schedule": CasetaSchedule(document.data())])
                    self.casetaSchedule.append(CasetaSchedule(document.data()))
                    self.schedulesTimes.append(document.data()["time"] as! String)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    @IBAction func devicesScenesAndSchedulesPressed(_ sender: Any) {
        self.devicesScrenesSchedules.changeUnderlinePosition()

        self.getFirebaseDevices()
        self.getFirebaseScenes()
        self.getFirebaseScedules()
        
        if devicesScrenesSchedules.selectedSegmentIndex == 0 {
            self.currentIndex = Constants.SegmentControlState.devices
            self.viewUnderDevices.backgroundColor = .lightBlue
            self.viewUnderScenes.backgroundColor = .clear
            self.viewUnderSchedules.backgroundColor = .clear
            tableView.reloadData()
        } else if devicesScrenesSchedules.selectedSegmentIndex == 1 {
            self.viewUnderDevices.backgroundColor = .clear
            self.viewUnderScenes.backgroundColor = .lightBlue
            self.viewUnderSchedules.backgroundColor = .clear
            self.currentIndex = Constants.SegmentControlState.scenes
            tableView.reloadData()
        } else if devicesScrenesSchedules.selectedSegmentIndex == 2 {
            self.viewUnderDevices.backgroundColor = .clear
            self.viewUnderScenes.backgroundColor = .clear
            self.viewUnderSchedules.backgroundColor = .lightBlue
            self.currentIndex = Constants.SegmentControlState.schedules
            tableView.reloadData()
        }
    }
    
    @objc func editSceneCellTapped(sender: UIButton) {
        let scenesForCaseta: CasetaScene = casetaSceneCurrentlySelected[sender.tag]["scene"] as! CasetaScene
        self.setSceneLights(devices: scenesForCaseta.devices, isOn: !scenesForCaseta.isOn)
        
        let editSceneVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.EditSceneVC) as! EditSceneVC
        editSceneVC.homeId = homeId
        editSceneVC.devicesForThisScene = scenesForCaseta.devices
        editSceneVC.devices = devicesInRoom
        editSceneVC.sceneName = casetaScene[sender.tag].name
        editSceneVC.sceneIconName = casetaScene[sender.tag].iconName
        let navController = UINavigationController(rootViewController: editSceneVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func editScheduleTapped(sender: UIButton) {
        let schedulesForCaseta: CasetaSchedule = casetaScheduleCurrentlySelected[sender.tag]["schedule"] as! CasetaSchedule
        self.setSceneLights(devices: schedulesForCaseta.devices, isOn: schedulesForCaseta.isOn)
        
        let editScheduleVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.EditScheduleVC) as! EditScheduleVC
        editScheduleVC.homeId = homeId
        editScheduleVC.devices = devicesInRoom
        editScheduleVC.scheduleName = casetaSchedule[sender.tag].name
        editScheduleVC.time = casetaSchedule[sender.tag].time
        editScheduleVC.daysOfWeek = casetaSchedule[sender.tag].daysOfWeek
        let navController = UINavigationController(rootViewController: editScheduleVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }

    @objc func selectedScheduleAsDefault(sender: UITapGestureRecognizer) {
        let db = Firestore.firestore()
        let scenesCollection = db.collection(Constants.FirestoreCollections.schedules)
        let query = scenesCollection.whereField("homeId", isEqualTo: homeId)

        let scheduleName = casetaSchedule[sender.view?.tag ?? 0]
        query.whereField("name", isEqualTo: String(String(scheduleName.name))).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : !scheduleName.isOn,
            ])
        }
        for (index, _) in casetaScheduleCurrentlySelected.enumerated() {
            if index == sender.view?.tag ?? 0 {
                casetaScheduleCurrentlySelected[index]["isSelected"] = true
                let casetaScene: CasetaSchedule = casetaScheduleCurrentlySelected[index]["schedule"] as! CasetaSchedule
                self.setSceneLights(devices: casetaScene.devices, isOn: !casetaScene.isOn)
            } else {
                casetaScheduleCurrentlySelected[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
    
    @objc func selectedSceneAsDefault(sender: UITapGestureRecognizer) {
        let db = Firestore.firestore()
        let scenesCollection = db.collection(Constants.FirestoreCollections.scenes)
        let query = scenesCollection.whereField("homeId", isEqualTo: homeId)

        let sceneName = casetaScene[sender.view?.tag ?? 0]
        query.whereField("name", isEqualTo: String(sceneName.name)).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : !sceneName.isOn,
            ])
        }
        for (index, _) in casetaSceneCurrentlySelected.enumerated() {
            if index == sender.view?.tag ?? 0 {
                casetaSceneCurrentlySelected[index]["isSelected"] = true
                let casetaScene: CasetaScene = casetaSceneCurrentlySelected[index]["scene"] as! CasetaScene
                self.setSceneLights(devices: casetaScene.devices, isOn: !casetaScene.isOn)
            } else {
                casetaSceneCurrentlySelected[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
    
    func setSceneLights(devices: [[String:Any]], isOn: Bool) {
        let db = Firestore.firestore()
        for device in devices {
            let documentId = device["deviceId"] as? String ?? ""
            let devicesCollection = db.collection(Constants.FirestoreCollections.devices)

            if isOn {
                devicesCollection.document(documentId).updateData([
                    "isOn" : true,
                    "brightness" : 100
                ])
            } else {
                devicesCollection.document(documentId).updateData([
                    "isOn" : false,
                    "brightness" : 0
                ])
            }
        }
    }
    
    @objc func turnScheduleOn(sender: UITapGestureRecognizer) {
        let casetaScheduleToChange = casetaSchedule[sender.view?.tag ?? 0]
        let isOn: Bool = casetaScheduleToChange.isOn
        let db = Firestore.firestore()
        db.collection("schedules").whereField("name", isEqualTo: String(casetaScheduleToChange.name)).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "isOn" : !isOn,
            ])
            self.tableView.reloadData()
        }
    }
    
}

extension CasetaSystemView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentIndex == Constants.SegmentControlState.devices {
            return devices.count
        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            return casetaScene.count
        } else {
            return casetaSchedule.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.currentIndex == Constants.SegmentControlState.devices {
            var deviceNames: [String] = []
            var deviceImageNames: [String] = []

            let cell = tableView.dequeueReusableCell(withIdentifier: "devicesCell") as! CasetaDevicesTableViewCell
            cell.roomNameLbl.text = roomIds[indexPath.row]
            let devicesInRoom: [CasetaDevice] = devices[roomIds[indexPath.row]] ?? []
            cell.numberOfCollectionViewCells = devicesInRoom.count
            for deviceName in devicesInRoom {
                deviceNames.append(deviceName.name)
                deviceImageNames.append(deviceName.iconName)
            }
            cell.devices = devicesInRoom
            cell.roomNameObjects = deviceNames
            cell.roomImageObjects = deviceImageNames
            cell.setUpCollectionViewCell()
            cell.collectionView.reloadData()

            return cell
            
        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            let cell = tableView.dequeueReusableCell(withIdentifier: "scenesCell") as! CasetaScenesTableViewCell
            cell.editImageBtn.tag = indexPath.row
            cell.editImageBtn.addTarget(self, action: #selector(editSceneCellTapped(sender:)), for: .touchUpInside)
            let cellIconName: String = casetaScene[indexPath.row].iconName ?? ""
            cell.roomImage.image = UIImage(named: cellIconName)?.imageWithInsets(insets: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20))?.withRenderingMode(.alwaysOriginal)
            let selectedCell: Bool = casetaScene[indexPath.row].isOn
            if selectedCell {
                cell.roomImage.backgroundColor = .lightBlue
                cell.roomNameLbl.textColor = .lightBlue
            } else {
                cell.roomImage.backgroundColor = .rgba(236, 240, 241,1.0)
                cell.roomNameLbl.textColor = .lightBlue
            }
            cell.roomImage.isUserInteractionEnabled = true
            cell.roomImage.tag = indexPath.row
            let cellImageTapped = UITapGestureRecognizer(target: self, action: #selector(selectedSceneAsDefault(sender:)))
            cell.roomImage.addGestureRecognizer(cellImageTapped)
            cell.roomNameLbl.text = casetaScene[indexPath.row].name
            cell.roomNameLbl.tag = indexPath.row
            let roomNameTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectedSceneAsDefault(sender:)))
            cell.roomNameLbl.addGestureRecognizer(roomNameTapGesture)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "schedulesCell") as! CasetaScheduleTableViewCell
            cell.editImageBtn.tag = indexPath.row
            cell.editImageBtn.addTarget(self, action: #selector(editScheduleTapped(sender:)), for: .touchUpInside)
            cell.scheduleName.text = casetaSchedule[indexPath.row].time.replacingOccurrences(of: " -5", with: "", options: .literal, range: nil)
            cell.scheduleDays.text = casetaSchedule[indexPath.row].name
            cell.scheduleDaysOfTheWeek.text = casetaSchedule[indexPath.row].daysOfWeek
            if casetaSchedule[indexPath.row].isOn {
                cell.scheduleName.backgroundColor = .lightBlue
                cell.scheduleName.textColor = .white
            } else {
                cell.scheduleName.backgroundColor = .lightGray
                cell.scheduleName.textColor = .black
            }
            
            let scheduleImageTurnedOnOff = UITapGestureRecognizer(target: self, action: #selector(turnScheduleOn(sender:)))
            cell.scheduleName.isUserInteractionEnabled = true
            cell.scheduleName.tag = indexPath.row
            cell.scheduleName.addGestureRecognizer(scheduleImageTurnedOnOff)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.currentIndex == Constants.SegmentControlState.devices {
            return 190
        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            return 100
        } else if self.currentIndex == Constants.SegmentControlState.schedules {
            return 100
        }
        return 100
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60))
        let headerText = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: 60))
        headerText.font = UIFont.boldSystemFont(ofSize: 19)
        headerText.textAlignment = .left
        headerText.textColor = .lightBlue
        headerText.isUserInteractionEnabled = true
        headerText.backgroundColor = .white
        let headerTapGesture = UITapGestureRecognizer(target: self, action: #selector(headerTapped))
        headerText.addGestureRecognizer(headerTapGesture)
        if self.currentIndex == Constants.SegmentControlState.devices {
            //headerText.text = " + Add Device"
        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            headerText.text = " + Add Scene"
        } else if self.currentIndex == Constants.SegmentControlState.schedules {
            headerText.text = " + Add Schedule"
        }
        headerView.addSubview(headerText)
        
        let footerView = UIView.init(frame: CGRect(x: 5, y: headerView.frame.maxY - 2, width: headerView.frame.width - 10, height: 0.5))
        footerView.backgroundColor = .lightGray
        headerView.addSubview(footerView)
        
        return headerView
    }
    
    @objc func headerTapped() {
        if self.currentIndex == Constants.SegmentControlState.devices {

        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            let sceneVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.PickANewSceneVC) as! PickANewSceneVC
            sceneVC.devices = devicesInRoom
            sceneVC.homeId = homeId
            let navController = UINavigationController(rootViewController: sceneVC)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        } else if self.currentIndex == Constants.SegmentControlState.schedules {
            let scheduleVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.DaysOfWeekScheduleVC) as! DaysOfWeekScheduleVC
            scheduleVC.homeId = homeId
            scheduleVC.devices = devicesInRoom
            let navController = UINavigationController(rootViewController: scheduleVC)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
    

    
    @objc func roomObjectTapped(sender: UIButton) {
        if sender.backgroundColor == UIColor(red: 82/255, green: 178/255, blue: 226/255, alpha: 1) {
            sender.backgroundColor = .lightGray
            sender.setImage(UIImage(systemName: "lightbulb.slash"), for: .normal)
            sender.tintColor = .black
        } else {
            sender.backgroundColor = UIColor(red: 82/255, green: 178/255, blue: 226/255, alpha: 1)
            sender.setImage(UIImage(systemName: "lightbulb.fill"), for: .normal)
            sender.tintColor = .white
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.currentIndex == Constants.SegmentControlState.devices {
            return 0
        } else if self.currentIndex == Constants.SegmentControlState.scenes {
            return 60
        } else if self.currentIndex == Constants.SegmentControlState.schedules {
            return 60
        }
        return 60
    }
}

