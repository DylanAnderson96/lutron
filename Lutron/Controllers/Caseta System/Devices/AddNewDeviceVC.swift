//
//  AddNewDeviceVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/22/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import FirebaseFirestoreSwift
import Firebase

protocol DidChangeDeviceIconDelegate {
    func changedIconImage(iconName: String)
}

class AddNewDeviceVC: UIViewController {

    var newDeviceType: [[String: Any]] = [
        ["name": "Ceiling Lights", "isSelected": true],
        ["name": "Pendants", "isSelected": false],
        ["name": "Chandelier", "isSelected": false],
        ["name": "Cabinet Lights", "isSelected": false],
        ["name": "Ceiling Fan Light", "isSelected": false],
        ["name": "Other", "isSelected": false]
    ]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    var homeId: String = ""
    var roomId: String = "Kitchen"
    var nameOfLight: String = "Ceiling Lights"
    var deviceId: String = ""
    var isEditingDevice: Bool = false
    var didChangeDeviceIconDelegate: DidChangeDeviceIconDelegate?
    var iconName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
        if self.isEditingDevice {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
        
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableHeaderView = UIView()
    }
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        if self.isEditingDevice {
            self.didChangeDeviceIconDelegate?.changedIconImage(iconName: nameOfLight)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Are You Sure?", message: "Would you like to add \(nameOfLight) to \(roomId)", alertType: .actionSheet, roomName: roomId)
    }

    func addDeviceToFirebase(roomId: String) {
        let db = Firestore.firestore()
        let documentUID = UUID()
        
        if nameOfLight == "Other" {
            iconName = "Ceiling Fan Light"
        } else {
            iconName = nameOfLight
        }
        let newDeviceData: [String: Any] = ["brightness": 0, "created": Date(), "createdBy": "iOS Device", "deviceId": deviceId, "homeId": homeId, "isOn": false, "name": nameOfLight, "iconName": iconName, "roomId": roomId]
        db.collection("devices").document(documentUID.uuidString).setData(newDeviceData) { (error) in
            if error != nil {
                print("There is an error saving the data to firebase")
            } else {
                let installAnotherDeviceVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.InstallNewDevices) as! InstallNewDevices
                installAnotherDeviceVC.homeId = self.homeId
                installAnotherDeviceVC.roomId = roomId
                installAnotherDeviceVC.nameOfDevice = self.nameOfLight
                let navController = UINavigationController(rootViewController: installAnotherDeviceVC)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        }
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style, roomName: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addTextField { (textfield) in
                textfield.placeholder = "Enter a name for this device."
            }
            let saveAction = UIAlertAction(title: "Save", style: .default) { (textfield) in
                let textfieldValue = alert.textFields![0] as UITextField
                if textfieldValue.text?.isEmpty ?? true {
                    self.displayAlertWithClosure(title: "Enter Display Name", message: "Please enter a name for this device.", alertType: .alert, roomName: self.roomId)
                } else {

                }
            }
            alert.addAction(saveAction)
        } else {
            alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (_) in
                self.addDeviceToFirebase(roomId: roomName)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
    
    @objc func cellSelected(sender: UIButton) {
        for (index, _) in newDeviceType.enumerated() {
            if index == sender.tag {
                newDeviceType[index]["isSelected"] = true
                self.nameOfLight = newDeviceType[index]["name"] as? String ?? ""
            } else {
                newDeviceType[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
}

extension AddNewDeviceVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newDeviceType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deviceTypeCell", for: indexPath) as! AddDeviceTableCell
        cell.header.setTitle(newDeviceType[indexPath.row]["name"] as? String, for: .normal)
        var newImagename: String = ""
        let imageName = newDeviceType[indexPath.row]["name"] as! String
        if imageName == "Other" {
            newImagename = "Ceiling Lights"
        } else {
            newImagename = imageName
        }
        cell.newDeviceImg.image = UIImage(named: newImagename)
        let isSelectedCell: Bool = newDeviceType[indexPath.row]["isSelected"] as? Bool ?? false
        if isSelectedCell {
            cell.selectedCellTick.image = UIImage(named: "checkMark")?.withRenderingMode(.alwaysTemplate)
            cell.selectedCellTick.tintColor = .link
        } else {
            cell.selectedCellTick.image = UIImage(named: "emptyCheckMark")?.withRenderingMode(.alwaysTemplate)
            cell.selectedCellTick.tintColor = .lightGray
        }
        cell.header.tag = indexPath.row
        cell.cellSelectedBtn.tag = indexPath.row
        cell.header.addTarget(self, action: #selector(cellSelected(sender:)), for: .touchUpInside)
        cell.cellSelectedBtn.addTarget(self, action: #selector(cellSelected(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index, _) in newDeviceType.enumerated() {
            if index == indexPath.row {
                newDeviceType[index]["isSelected"] = true
                self.nameOfLight = newDeviceType[index]["name"] as? String ?? ""
            } else {
                newDeviceType[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
}
