//
//  PairDevice.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/23/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class PairDevice: CoreBaseController {

    var homeId: String = ""
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    var deviceId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listenForFirebasePairing()
        
    }
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func listenForFirebasePairing() {
        let db = Firestore.firestore()
        db.collection("pairings").document("1").addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("there was an error getting the data for the firestore")
            } else {
                let documents = snapshot?.data() ?? [:]
                if self.deviceId.isEmpty {
                    self.deviceId = documents["deviceId"] as? String ?? ""
                } else {
                    self.moveToSelectRoom()
                }
            }
        }
    }
    
    func moveToSelectRoom() {
        let selectRooms = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SelectRoomForDeviceVC) as! SelectRoomForDeviceVC
        selectRooms.modalPresentationStyle = .fullScreen
        selectRooms.homeId = homeId
        selectRooms.deviceId = deviceId
        self.navigationController?.pushViewController(selectRooms, animated: true)
    }

}
