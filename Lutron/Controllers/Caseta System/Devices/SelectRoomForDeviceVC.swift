//
//  SelectRoomForDeviceVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/22/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

protocol UpdateRoomDelegate {
    func newRoom(roomName: String)
}

class SelectRoomForDeviceVC: UIViewController {

    var roomNames: [[String: Any]] = [
        ["name": "Kitchen", "isSelected": true],
        ["name": "Living Room", "isSelected": false],
        ["name": "Dining Room", "isSelected": false],
        ["name": "Master Bedroom", "isSelected": false],
        ["name": "Front Porch", "isSelected": false],
        ["name": "Foyer/Entry", "isSelected": false],
        ["name": "Exterior", "isSelected": false],
        ["name": "Family Room", "isSelected": false],
        ["name": "Garage", "isSelected": false],
        ["name": "Office", "isSelected": false],
        ["name": "Bedroom", "isSelected": false],
        ["name": "Bathroom", "isSelected": false],
        ["name": "Basement", "isSelected": false],
        ["name": "Hallway/Stairs", "isSelected": false]
    ]
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var nextbtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var currentRooms: [String] = []
    var lightFixtureType: String = ""
    var homeId: String = ""
    var nameOfLight: String = ""
    var roomId: String = "Kitchen"
    var isChangingRooms: Bool = false
    var updateRoomDelegate: UpdateRoomDelegate?
    var deviceId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()

        self.topLabel.adjustsFontSizeToFitWidth = true
        self.topLabel.text = "Select the room where you'd like to add this device."
        
    }
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func nextBtnPressed(_ sender: Any) {
        //go to add device type
        if isChangingRooms {
            updateRoomDelegate?.newRoom(roomName: self.roomId)
            self.dismiss(animated: true, completion: nil)
        } else {
            let addNewDeviceVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.AddNewDeviceVC) as! AddNewDeviceVC
            addNewDeviceVC.homeId = homeId
            addNewDeviceVC.roomId = roomId
            addNewDeviceVC.deviceId = deviceId
            let navController = UINavigationController(rootViewController: addNewDeviceVC)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func cellSelected(sender: UIButton) {
        for (index, _) in roomNames.enumerated() {
            if index == sender.tag {
                roomNames[index]["isSelected"] = true
                self.roomId = roomNames[index]["name"] as? String ?? ""
            } else {
                roomNames[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
    
}

extension SelectRoomForDeviceVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "roomTypeCell", for: indexPath) as! SelectRoomTableCell
        
        cell.roomNameBtnText.setTitle(roomNames[indexPath.row]["name"] as? String, for: .normal)
        let isSelectedCell: Bool = roomNames[indexPath.row]["isSelected"] as? Bool ?? false
        if isSelectedCell {
            cell.selectCellTick.image = UIImage(named: "checkMark")?.withRenderingMode(.alwaysTemplate)
            cell.selectCellTick.tintColor = .link
        } else {
            cell.selectCellTick.image = UIImage(named: "emptyCheckMark")?.withRenderingMode(.alwaysTemplate)
            cell.selectCellTick.tintColor = .lightGray
        }
        cell.roomNameBtnText.tag = indexPath.row
        cell.cellSelectedBtn.tag = indexPath.row
        cell.roomNameBtnText.addTarget(self, action: #selector(cellSelected(sender:)), for: .touchUpInside)
        cell.cellSelectedBtn.addTarget(self, action: #selector(cellSelected(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index, _) in roomNames.enumerated() {
            if index == indexPath.row {
                roomNames[index]["isSelected"] = true
                self.roomId = roomNames[index]["name"] as? String ?? ""
            } else {
                roomNames[index]["isSelected"] = false
            }
        }
        self.tableView.reloadData()
    }
    
}
