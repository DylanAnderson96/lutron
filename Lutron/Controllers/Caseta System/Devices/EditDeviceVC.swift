//
//  EditDeviceVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/23/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class EditDeviceVC: CoreBaseController, UpdateRoomDelegate, DidChangeDeviceIconDelegate {
    
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var deviceNameLbl: UILabel!
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var roomNameBtn: UIButton!
    @IBOutlet weak var deleteDeviceBtn: UIButton!
    var currentDevice: CasetaDevice!
    var newRoom: String = ""
    @IBOutlet weak var iconImage: UIButton!
    var imageName: String = ""
    var newImage: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
        
    }
    
    func setUpVisuals() {
        self.deviceName.clearButtonMode = .always
        self.deviceName.text = currentDevice.name
        self.deviceNameLbl.text = currentDevice.roomID
        if newRoom.isEmpty {
            self.roomNameBtn.setTitle(currentDevice.roomID, for: .normal)
        } else {
            self.roomNameBtn.setTitle(newRoom, for: .normal)
        }
        
        var imageName: String = ""
        if currentDevice.iconName == "Other" {
            imageName = "Ceiling Lights"
        } else if currentDevice.iconName == "doors" {
            imageName = "Cabinet Lights"
        } else if currentDevice.iconName == "pendants" {
            imageName = "Pendants"
        } else if currentDevice.iconName == "chandelier" {
            imageName = "Chandelier"
        } else if currentDevice.iconName == "ceilingLights" {
            imageName = "Ceiling Lights"
        } else if currentDevice.iconName == "lightbulb" {
            imageName = "theLightbulb"
        } else {
            imageName = currentDevice.iconName
        }
        
        if currentDevice.iconName == "Cabinet Lights" {
            newImage = "doors"
        } else if currentDevice.iconName == "Pendants" {
            newImage = "pendants"
        } else if currentDevice.iconName == "Chandelier" {
            newImage = "chandelier"
        } else if currentDevice.iconName == "Ceiling Lights" {
            newImage = "ceilingLights"
        } else if currentDevice.iconName == "Ceiling Fan Light" {
            newImage = "lightbulb"
        } else {
            newImage = currentDevice.iconName
        }
        
        self.iconImage.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    func newRoom(roomName: String) {
        self.newRoom = roomName
        self.roomNameBtn.setTitle(newRoom, for: .normal)
    }
    
    @IBAction func roomNamePressed(_ sender: Any) {
        let selectRoomVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SelectRoomForDeviceVC) as! SelectRoomForDeviceVC
        selectRoomVC.isChangingRooms = true
        selectRoomVC.updateRoomDelegate = self
        let navController = UINavigationController(rootViewController: selectRoomVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func iconTypePressed(_ sender: Any) {
        let editDeviceIconVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.AddNewDeviceVC) as! AddNewDeviceVC
        editDeviceIconVC.didChangeDeviceIconDelegate = self
        editDeviceIconVC.isEditingDevice = true
        editDeviceIconVC.deviceId = currentDevice.deviceID
        let navController = UINavigationController(rootViewController: editDeviceIconVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    func changedIconImage(iconName: String) {
        if iconName == "Other" {
            imageName = "Ceiling Lights"
        } else if iconName == "doors" {
            imageName = "Cabinet Lights"
        } else if iconName == "pendants" {
            imageName = "Pendants"
        } else if iconName == "chandelier" {
            imageName = "Chandelier"
        } else if iconName == "ceilingLights" {
            imageName = "Ceiling Lights"
        } else if iconName == "lightbulb" {
            imageName = "theLightbulb"
        } else {
            imageName = iconName
        }
        
        if iconName == "Cabinet Lights" {
            newImage = "doors"
        } else if iconName == "Pendants" {
            newImage = "pendants"
        } else if iconName == "Chandelier" {
            newImage = "chandelier"
        } else if iconName == "Ceiling Lights" {
            newImage = "ceilingLights"
        } else if iconName == "Ceiling Fan Light" {
            newImage = "lightbulb"
        } else {
            newImage = iconName
        }
        self.iconImage.setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    @IBAction func savePressed(_ sender: Any) {
        if deviceName.text?.isEmpty ?? true {
            self.displayAlert(title: "Whoops", message: "Please enter in a name to continue")
        } else {
            self.updateFirebase()
        }
    }
    
    func updateFirebase() {
        let db = Firestore.firestore()
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDevice.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "name" : String(self.deviceName.text ?? "Ceiling Fan Light"),
                "roomId" : String(self.roomNameBtn.titleLabel?.text ?? "Kitchen"),
                "iconName": String(self.newImage)
            ])
            self.performSegue(withIdentifier: "segueToMainDevices", sender: nil)
        }
    }
    
    func removeFromFirebase() {
        let db = Firestore.firestore()
//        var devicesInScenes = [[String: Any]]()
//        var collectionId: String = ""
        
        db.collection("devices").whereField("deviceId", isEqualTo: currentDevice.deviceID as String).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.delete()
        }
//        let scenesCollection = db.collection(Constants.FirestoreCollections.scenes)
//        let query = scenesCollection.whereField("homeId", isEqualTo: String(currentDevice.homeID))
//        query.getDocuments { (snapshot, error) in
//            if error != nil {
//                print("error getting devices in scenes")
//            } else {
//                for document in snapshot!.documents {
//                    collectionId = document.documentID
//                    let devices: [[String:Any]] = document.data()["devices"] as? [[String : Any]] ?? [[:]]
//                    for device in devices {
//                        if device["deviceId"] as? String != self.currentDevice.deviceID {
//                            devicesInScenes.append(device)
//                        } else {
//                            continue
//                        }
//                    }
//                }
//            }
//        }
//        
//        db.collection("scenes").document(collectionId).getDocument { (snapshot, error) in
//            if error != nil {
//                print("Error getting document for device in scenes")
//            } else {
//                snapshot?.reference.updateData(["devices": devicesInScenes])
//            }
//        }
        self.performSegue(withIdentifier: "segueToMainDevices", sender: nil)
    }
    
    @IBAction func deleteDevicePressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Delete \(currentDevice.name!)", message: "Are you sure you would like to delete this?", alertType: .actionSheet)
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.removeFromFirebase()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }

}
