//
//  InstallNewDevices.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/23/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class InstallNewDevices: CoreBaseController {

    @IBOutlet weak var nameOfLightAdded: UILabel!
    @IBOutlet weak var addAnotherDeviceBtn: UIButton!
    @IBOutlet weak var imDoneInstallingbtn: UIButton!
    var homeId: String = ""
    var roomId: String = ""
    var nameOfDevice: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addAnotherDeviceBtn.layer.cornerRadius = 8
        self.nameOfLightAdded.text = "\(roomId) \(nameOfDevice) added!"
    }

    @IBAction func addAnotherDevicePressed(_ sender: Any) {
        let pairDeviceVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.PairDevice) as! PairDevice
        pairDeviceVC.homeId = homeId
        let navController = UINavigationController(rootViewController: pairDeviceVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func imDoneInstallingBtnPressed(_ sender: Any) {
        //completetion closure to unwind back to devices screen
        self.performSegue(withIdentifier: "selectedRoomSegue", sender: nil)

    }
    
}
