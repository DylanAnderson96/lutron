//
//  ScheduleTimeAndDate.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/28/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

protocol ChangeTimeDelegate {
    func didUpdateTime(newTime: String)
}

class ScheduleTimeAndDate: CoreBaseController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var timeOrSunriseSunset: UISegmentedControl!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var stackViewSunrise: UIStackView!
    @IBOutlet weak var beforeAfterSunrise: UIPickerView!
    var currentIndex: Int = 0
    var beforeAfterSunriseOptions = [["10 min before", "5 min before", "At", "5 min after", "10 min after"], ["Sunrise", "Sunset"]]
    var homeId: String = ""
    var devices: [CasetaDevice] = []
    var daysOfTheWeekSchedule = String()
    var isChangingCurrentDate: Bool = false
    var changeTimeDelegate: ChangeTimeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupVisuals()
        //Delegates
        self.beforeAfterSunrise.delegate = self
        self.beforeAfterSunrise.dataSource = self
        self.beforeAfterSunrise.selectRow(2, inComponent: 0, animated: false)
        self.beforeAfterSunrise.tag = 1
    }
    
    func setupVisuals() {
        self.timePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        self.timeLabel.text = ""
        self.nextBtn.isEnabled = false
        
        if self.isChangingCurrentDate {
            self.nextBtn.tintColor = .clear
        } else {
            self.nextBtn.tintColor = .darkGray
        }
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        let scheduleNameVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ScheduleNameVC) as! ScheduleNameVC
        scheduleNameVC.homeId = homeId
        scheduleNameVC.devices = devices
        scheduleNameVC.daysOfTheWeekSchedule = daysOfTheWeekSchedule
        scheduleNameVC.timeOfSchedule = String(timeLabel.text!) + " -5"
        let navController = UINavigationController(rootViewController: scheduleNameVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        if self.isChangingCurrentDate {
            changeTimeDelegate?.didUpdateTime(newTime: timeLabel.text!)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func setTimeStateSunrise(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.currentIndex = 0
            self.stackViewSunrise.isHidden = true
            self.timePicker.isHidden = false
        } else {
            self.currentIndex = 1
            self.stackViewSunrise.isHidden = false
            self.timePicker.isHidden = true
        }
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let date = sender.date
        let stringOfDate = date.toString(dateFormat: "h:mm a")
        self.timeLabel.text = stringOfDate
        if self.isChangingCurrentDate {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return beforeAfterSunriseOptions[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return self.beforeAfterSunriseOptions[component][row]
        } else {
            return self.beforeAfterSunriseOptions[component][row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let time = beforeAfterSunriseOptions[0][pickerView.selectedRow(inComponent: 0)]
        let sunrise = beforeAfterSunriseOptions[1][pickerView.selectedRow(inComponent: 1)]

        self.timeLabel.text = time + " " + sunrise
        if self.isChangingCurrentDate {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
}
