//
//  EditScheduleVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/29/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class EditScheduleVC: CoreBaseController, DidChangeDaysOfWeekDelegate, ChangeTimeDelegate {

    @IBOutlet weak var scheduleNameTF: UITextField!
    @IBOutlet weak var scheduleTimeLbl: UIButton!
    @IBOutlet weak var deviceLbl: UIButton!
    @IBOutlet weak var devicesShadesAudioLbl: UIButton!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var deleteSchedule: UIButton!
    @IBOutlet weak var isEnabledLbl: UIButton!
    @IBOutlet weak var isEnabledSwitch: UISwitch!
    
    var scheduleName: String = ""
    var homeId: String = ""
    var devicesForThisScene = [[String: Any]]()
    var time: String = ""
    var daysOfWeek: String = ""    
    var devices: [CasetaDevice] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
        
    }
    
    func setUpVisuals() {
        self.scheduleNameTF.text = scheduleName
        self.scheduleNameTF.clearButtonMode = .always
        self.scheduleTimeLbl.setTitle(String(time).replacingOccurrences(of: " -5", with: "", options: .literal, range: nil), for: .normal)
        self.scheduleTimeLbl.titleLabel?.adjustsFontSizeToFitWidth = true
        self.devicesShadesAudioLbl.setTitle(daysOfWeek, for: .normal)
    }
    
    func updateFirebase() {
        let db = Firestore.firestore()
        guard let newScheduleName = scheduleNameTF.text else { return }
        db.collection(Constants.FirestoreCollections.schedules).whereField("name", isEqualTo: scheduleName).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "name" : newScheduleName,
                "time" : String((self.scheduleTimeLbl.titleLabel?.text!)!) + " -5",
                "daysOfWeek": self.daysOfWeek,
                "isOn": self.isEnabledSwitch.isOn
            ])
            self.performSegue(withIdentifier: "segueToEditSchedules", sender: nil)
        }
    }
    
    func deleteFromFirebase() {
        let db = Firestore.firestore()
        
        db.collection(Constants.FirestoreCollections.schedules).whereField("name", isEqualTo: scheduleName).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.delete()
        }
        self.performSegue(withIdentifier: "segueToEditSchedules", sender: nil)
    }
    
    func newDays(newDays: String) {
        self.devicesShadesAudioLbl.setTitle(newDays, for: .normal)
        self.daysOfWeek = newDays
    }
    
    func didUpdateTime(newTime: String) {
        self.scheduleTimeLbl.setTitle(newTime.replacingOccurrences(of: "-5", with: "", options: .literal, range: nil), for: .normal)
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Success", message: "Schedules has been updated!", alertType: .alert)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeScheduleTimePressed(_ sender: Any) {
        let changeTimeVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ScheduleTimeAndDate) as! ScheduleTimeAndDate
        changeTimeVC.changeTimeDelegate = self
        changeTimeVC.isChangingCurrentDate = true
        let navController = UINavigationController(rootViewController: changeTimeVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)

    }
    
    @IBAction func changeDaysOfWeek(_ sender: Any) {
        let daysOfWeekVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.DaysOfWeekScheduleVC) as! DaysOfWeekScheduleVC
        daysOfWeekVC.didChangeDaysDelegate = self
        daysOfWeekVC.isChangingDays = true
        let navController = UINavigationController(rootViewController: daysOfWeekVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func changeDevicePressed(_ sender: Any) {
        let setScenesVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SetSceneDefaultsVC) as! SetSceneDefaultsVC
        setScenesVC.isEditingDevicesForSchedules = true
        setScenesVC.devices = devices
        setScenesVC.scheduleName = scheduleName
        setScenesVC.homeId = homeId
        let navController = UINavigationController(rootViewController: setScenesVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func deleteSchedulePressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Are you sure?", message: "Please confirm to delete this schedule.", alertType: .actionSheet)
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.updateFirebase()
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.deleteFromFirebase()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
}

