//
//  ScheduleNameVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/29/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ScheduleNameVC: CoreBaseController, UITextFieldDelegate {
    
    @IBOutlet weak var scheduleNameTF: UITextField!
    @IBOutlet weak var nameScheduleLbl: UILabel!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    var nameOfSchedule: String = ""
    var homeId: String = ""
    var devices: [CasetaDevice] = []
    var daysOfTheWeekSchedule = String()
    var timeOfSchedule: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scheduleNameTF.delegate = self
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let scheduleName = scheduleNameTF.text else { return }
        if scheduleName.isEmpty {
            self.nextBtn.isEnabled = false
        } else {
            self.nextBtn.isEnabled = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let scheduleName = scheduleNameTF.text else { return true }
        if scheduleName.isEmpty {
            self.nextBtn.isEnabled = false
        } else {
            self.nextBtn.isEnabled = true
        }
        return true
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        guard let scheduleName = scheduleNameTF.text else { return }
        self.nameOfSchedule = scheduleName
        
        let setScheduleDefaultsVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SetSceneDefaultsVC) as! SetSceneDefaultsVC
        setScheduleDefaultsVC.isPickingDevicesSchedules = true
        setScheduleDefaultsVC.homeId = homeId
        setScheduleDefaultsVC.scheduleName = scheduleName
        setScheduleDefaultsVC.devices = devices
        setScheduleDefaultsVC.daysOfTheWeekSchedule = daysOfTheWeekSchedule
        setScheduleDefaultsVC.timeOfSchedule = timeOfSchedule
        let navController = UINavigationController(rootViewController: setScheduleDefaultsVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true)

    }
}
