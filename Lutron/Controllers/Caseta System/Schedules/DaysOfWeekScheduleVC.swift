//
//  DaysOfWeekScheduleVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/28/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

protocol DidChangeDaysOfWeekDelegate {
    func newDays(newDays: String)
}

class DaysOfWeekScheduleVC: CoreBaseController {
    
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var selectDaysLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var homeId: String = ""
    var devices: [CasetaDevice] = []
    var isChangingDays: Bool = false
    var didChangeDaysDelegate: DidChangeDaysOfWeekDelegate?
    var daysOfWeek: String = ""

    var daysOfTheWeek: [[String:Any]] = [
        ["daysOfWeek":"Sunday", "isSelected": true],
        ["daysOfWeek":"Monday", "isSelected": true],
        ["daysOfWeek":"Tuesday", "isSelected": true],
        ["daysOfWeek":"Wednesday", "isSelected": true],
        ["daysOfWeek":"Thursday", "isSelected": true],
        ["daysOfWeek":"Friday", "isSelected": true],
        ["daysOfWeek":"Saturday", "isSelected": true]
    ]
    var daysOfWeekDict: [String: Int] = ["Su": 0, "Mo": 1, "Tu": 2, "We": 3, "Th": 4, "Fr": 5, "Sa" : 6]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()
        
        if self.isChangingDays {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
        
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let daysOfWeekSorted = Array(daysOfWeekDict.keys)
        let sortedArray = daysOfWeekSorted.sorted(by: { daysOfWeekDict[$0]! < daysOfWeekDict[$1]! })
        for day in sortedArray {
            daysOfWeek += " \(day)"
        }
        let scheduleTimeAndDate = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ScheduleTimeAndDate) as! ScheduleTimeAndDate
        scheduleTimeAndDate.homeId = homeId
        scheduleTimeAndDate.devices = devices
        scheduleTimeAndDate.daysOfTheWeekSchedule = daysOfWeek
        let navController = UINavigationController(rootViewController: scheduleTimeAndDate)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        if self.isChangingDays {
            let daysOfWeekSorted = Array(daysOfWeekDict.keys)
            let sortedArray = daysOfWeekSorted.sorted(by: { daysOfWeekDict[$0]! < daysOfWeekDict[$1]! })
            for day in sortedArray {
                daysOfWeek += " \(day)"
            }
            didChangeDaysDelegate?.newDays(newDays: daysOfWeek)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func daySelected(sender: UIButton) {
        let state: Bool = daysOfTheWeek[sender.tag]["isSelected"] as? Bool ?? false
        let name: String = daysOfTheWeek[sender.tag]["daysOfWeek"] as? String ?? ""
        daysOfTheWeek[sender.tag]["isSelected"] = !state
        if state {
            if self.daysOfWeekDict.keys.contains(String(name.prefix(2))) {
                self.daysOfWeekDict.removeValue(forKey: String(name.prefix(2)))
            }
        } else {
            if !self.daysOfWeekDict.keys.contains(String(name.prefix(2))) {
                self.daysOfWeekDict[String(name.prefix(2))] = sender.tag
            }
        }
        self.tableView.reloadData()
    }
    
    @objc func checkmarkTapped(sender: UITapGestureRecognizer) {
        guard let imageView = sender.view as? UIImageView else {
            print("Error not an image view")
            return
        }
        let state: Bool = daysOfTheWeek[imageView.tag]["isSelected"] as? Bool ?? false
        let name: String = daysOfTheWeek[imageView.tag]["daysOfWeek"] as? String ?? ""
        daysOfTheWeek[imageView.tag]["isSelected"] = !state
        if state {
            if self.daysOfWeekDict.keys.contains(String(name.prefix(2))) {
                self.daysOfWeekDict.removeValue(forKey: String(name.prefix(2)))
            }
        } else {
            if !self.daysOfWeekDict.keys.contains(String(name.prefix(2))) {
                self.daysOfWeekDict[String(name.prefix(2))] = imageView.tag
            }
        }
        self.tableView.reloadData()
    }
    
}

extension DaysOfWeekScheduleVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return daysOfTheWeek.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let checkmarkImage = UIImage(named: "checkMark")?.withRenderingMode(.alwaysTemplate)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "daysOfTheWeekCell", for: indexPath) as! DaysOfTheWeekTableViewCell
        
        let cellTapGesture = UITapGestureRecognizer(target: self, action: #selector(checkmarkTapped(sender:)))
        cell.isSelectedImage.layer.cornerRadius = cell.isSelectedImage.frame.width/2
        cell.isSelectedImage.contentMode = .scaleAspectFit
        cell.isSelectedImage.tag = indexPath.row
        cell.isSelectedImage.addGestureRecognizer(cellTapGesture)
        cell.isSelectedImage.isUserInteractionEnabled = true
        
        let isDaySelected: Bool = daysOfTheWeek[indexPath.row]["isSelected"] as? Bool ?? true

        if isDaySelected {
            cell.isSelectedImage.image = checkmarkImage
            cell.isSelectedImage.tintColor = .link
            cell.isSelectedImage.layer.borderWidth = 0.0
            cell.isSelectedImage.layer.borderColor = UIColor.clear.cgColor
        } else {
            cell.isSelectedImage.image = UIImage(named: "emptyCheckMark")
            cell.isSelectedImage.tintColor = .lightGray
            cell.isSelectedImage.layer.borderWidth = 1.0
            cell.isSelectedImage.layer.borderColor = UIColor.black.cgColor
        }
        
        cell.daysOfTheWeekBtn.tag = indexPath.row
        cell.daysOfTheWeekBtn.setTitle(daysOfTheWeek[indexPath.row]["daysOfWeek"] as? String, for: .normal)
        cell.daysOfTheWeekBtn.addTarget(self, action: #selector(daySelected(sender:)), for: .touchUpInside)

        let bgColorView = UIView()
        bgColorView.backgroundColor = .white
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    
}
