//
//  SetSceneDefaultsVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/23/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class SetSceneDefaultsVC: CoreBaseController {

    var selectedIcon = UIImage()
    var imageName: String = ""
    var sceneName: String = ""
    var scheduleName: String = ""
    
    @IBOutlet weak var sceneNameLbl: UILabel!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var devices: [CasetaDevice] = []
    var selectedIndexPath: IndexPath? = nil
    var devicesForThisScene = [[String: Any]]()
    var currentIndexPath: Int!
    var homeId: String = ""
    var isChangingRoomsForScene: Bool = false
    var deviceNames = [String]()
    
    var isPickingDevicesSchedules: Bool = false
    var isEditingDevicesForSchedules: Bool = false
    var daysOfTheWeekSchedule = String()
    var timeOfSchedule: String = ""

    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
        self.getDeviceNames()
    }
    
    func setUpVisuals() {
        if isChangingRoomsForScene {
            self.tableViewTopConstraint.constant = -64
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
//            for device in devices {
//                var deviceState: Bool = false
//                if device.isOn == Constants.DeviceState.ON {
//                    deviceState = true
//                } else {
//                    deviceState = false
//                }
//                let dict: [String: Any] = ["device": device, "isAdded": deviceState, "isOn": deviceState, "brightness": Int(device.brightness)]
//                devicesForThisScene.append(dict)
//            }
        } else if isEditingDevicesForSchedules {
            self.tableViewTopConstraint.constant = -64
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
         } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link

            self.tableViewTopConstraint.constant = 8
            
            if isPickingDevicesSchedules {
                self.sceneNameLbl.text = "Choose the devices and set their levels for the schedule you would like to create."
                self.navigationItem.title = scheduleName
            } else {
                self.sceneNameLbl.text = "Choose the devices and set their levels for the scene you would like to create."
                self.navigationItem.title = sceneName
            }
            
        }
        for device in devices {
            var deviceState: Bool = false
            if device.isOn == Constants.DeviceState.ON {
                deviceState = true
            } else {
                deviceState = false
            }
            let dict: [String: Any] = ["device": device, "isAdded": deviceState, "isOn": deviceState, "brightness": Int(device.brightness)]
            devicesForThisScene.append(dict)
        }
    }
    
    func getDeviceNames() {
        let db = Firestore.firestore()
        let scenesCollection = db.collection(Constants.FirestoreCollections.devices)
        let query = scenesCollection.whereField("homeId", isEqualTo: homeId)
        query.addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("There was an error getting the data from Firebase rooms \(String(describing: error?.localizedDescription))")
            } else {
                self.deviceNames.removeAll()
                
                guard let documents = snapshot?.documents else { return }
                for document in documents {
                    let deviceName = document.data()["name"] as? String ?? ""
                    self.deviceNames.append(deviceName)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        if self.isChangingRoomsForScene {
            self.displayAlertWithClosure(title: "Are you sure?", message: "Save changes made to \(sceneName)?", alertType: .actionSheet)
        } else if self.isEditingDevicesForSchedules {
            self.displayAlertWithClosure(title: "Are you sure?", message: "Save changes made to \(scheduleName)?", alertType: .actionSheet)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        if self.isPickingDevicesSchedules {
            self.displayAlertWithClosure(title: "Are you sure?", message: "Save \(scheduleName) as a new schedule?", alertType: .actionSheet)
        } else {
            self.displayAlertWithClosure(title: "Are you sure?", message: "Save \(sceneName) as a new scene?", alertType: .actionSheet)
        }
    }
    
    func saveDataToFirebase() {
        let documentUUID = UUID()
        let db = Firestore.firestore()
        var allDevices = [[String: Any]]()
        if self.isChangingRoomsForScene {
            db.collection("scenes").whereField("name", isEqualTo: sceneName).getDocuments { (snapshot, error) in
                let document = snapshot?.documents.first
                document?.reference.updateData([
                    "devices" : self.devicesForThisScene
                ])
                self.dismiss(animated: true, completion: nil)
            }
        } else if self.isEditingDevicesForSchedules {
            db.collection("schedules").whereField("name", isEqualTo: scheduleName).getDocuments { (snapshot, error) in
                let document = snapshot?.documents.first
                document?.reference.updateData([
                    "devices" : self.devicesForThisScene
                ])
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            for (index, _) in devicesForThisScene.enumerated() {
                if self.devicesForThisScene[index]["isAdded"] as? Bool ?? false {
                    let casetaDevice: CasetaDevice = self.devicesForThisScene[index]["device"] as! CasetaDevice
                    let isOn: Bool = self.devicesForThisScene[index]["isOn"] as! Bool
                    let brightness = self.devicesForThisScene[index]["brightness"] as! Int
                    allDevices.append(["brightness": brightness, "deviceId": String(casetaDevice.deviceDocId), "isOn": isOn])
                }
            }
            if isPickingDevicesSchedules {
                let newSchedule: [String: Any] = ["created": Date(), "createdBy": "iOS Device", "devices": allDevices, "homeId": homeId, "name": scheduleName, "isOn": false, "daysOfWeek": daysOfTheWeekSchedule, "time": timeOfSchedule]
                db.collection(Constants.FirestoreCollections.schedules).document(documentUUID.uuidString).setData(newSchedule) { (error) in
                    if error != nil {
                        print("There is an error saving the data to firebase")
                    } else {
                        self.displayAlertWithClosure(title: "Success", message: "New schedule saved!", alertType: .alert)
                    }
                }
            } else {
                let newScene: [String: Any] = ["created": Date(), "createdBy": "iOS Device", "devices": allDevices, "homeId": homeId, "iconName": imageName, "name": sceneName, "isOn": false]
                db.collection("scenes").document(documentUUID.uuidString).setData(newScene) { (error) in
                    if error != nil {
                        print("There is an error saving the data to firebase")
                    } else {
                        self.displayAlertWithClosure(title: "Success", message: "New scene saved!", alertType: .alert)
                    }
                }
            }
        }
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                if self.isChangingRoomsForScene {
                    self.dismiss(animated: true, completion: nil)
                } else if self.isEditingDevicesForSchedules {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    if self.isPickingDevicesSchedules {
                        self.performSegue(withIdentifier: "segueToSchedules", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "segueToScenes", sender: nil)
                    }
                }
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.saveDataToFirebase()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                self.dismiss(animated: true, completion: nil)
            }))
        }
        self.present(alert, animated: true)
    }
    
    @IBAction func extendCellBtnPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func turnCurrentDeviceOn(_ sender: UIButton) {
        self.devicesForThisScene[currentIndexPath]["isOn"] = true
        self.devicesForThisScene[currentIndexPath]["brightness"] = 100
        self.devicesForThisScene[currentIndexPath]["isAdded"] = true
        self.tableView.reloadData()
    }
    
    @IBAction func increaseCurrentDeviceBrightness(_ sender: UIButton) {
        let brightness: Int = self.devicesForThisScene[currentIndexPath]["brightness"] as? Int ?? 1
        if brightness < 100 {
            self.devicesForThisScene[currentIndexPath]["brightness"] = (brightness + 1)
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = true
        } else {
            self.devicesForThisScene[currentIndexPath]["brightness"] = 100
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = true
        }
        self.tableView.reloadData()
    }
    
    @IBAction func decreaseCurrentDeviceBrightness(_ sender: UIButton) {
        let brightness: Int = self.devicesForThisScene[currentIndexPath]["brightness"] as? Int ?? 1
        if brightness > 1 {
            self.devicesForThisScene[currentIndexPath]["brightness"] = (brightness - 1)
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = true
        } else {
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = false
            self.devicesForThisScene[currentIndexPath]["brightness"] = 0
        }
        self.tableView.reloadData()
    }
    
    @IBAction func turnCurrentDeviceOff(_ sender: UIButton) {
        self.devicesForThisScene[currentIndexPath]["isOn"] = false
        self.devicesForThisScene[currentIndexPath]["brightness"] = 0
        self.devicesForThisScene[currentIndexPath]["isAdded"] = true
        self.tableView.reloadData()
    }
    
    @IBAction func setCurrentDeviceSlider(_ sender: UISlider) {
        self.devicesForThisScene[currentIndexPath]["brightness"] = Int(sender.value * 100)
        if Int(sender.value * 100) > 0 {
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = true
        } else {
            self.devicesForThisScene[currentIndexPath]["isAdded"] = true
            self.devicesForThisScene[currentIndexPath]["isOn"] = false
        }
        self.tableView.reloadData()
    }
    
}

extension SetSceneDefaultsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isChangingRoomsForScene {
            return deviceNames.count
        } else if self.isEditingDevicesForSchedules{
            return deviceNames.count
        } else {
            return devices.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectDevicesCell", for: indexPath) as! PickDevicesForSceneCell
        let selectionImage = UIImage(named: "checkMark")?.withRenderingMode(.alwaysTemplate)
        let brightnessValue: Int = devicesForThisScene[indexPath.row]["brightness"] as? Int ?? 0
        cell.extendCellBtn.tag = indexPath.row
        let deviceFloatValue = Float(brightnessValue)/Float(100)
        cell.slider.setValue(Float(deviceFloatValue), animated: false)
        if self.isChangingRoomsForScene {
            cell.nameOfLight.text = deviceNames[indexPath.row]
        } else if self.isEditingDevicesForSchedules {
            cell.nameOfLight.text = deviceNames[indexPath.row]
        } else {
            cell.nameOfLight.text = devices[indexPath.row].name
        }
        if brightnessValue > 0 && brightnessValue < 100 {
            cell.deviceStateLbl.text = "\(brightnessValue) %"
            cell.isSelectedDevice.setImage(selectionImage, for: .normal)
            cell.isSelectedDevice.tintColor = .link
        } else if brightnessValue == 100 {
            cell.deviceStateLbl.text = "On"
            cell.isSelectedDevice.setImage(selectionImage, for: .normal)
            cell.isSelectedDevice.tintColor = .link
        } else {
            cell.deviceStateLbl.text = "Off"
            cell.isSelectedDevice.setImage(UIImage(named: "emptyCheckMark"), for: .normal)
            cell.isSelectedDevice.tintColor = .lightGray
        }
        
        cell.deviceDimemrUp.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        cell.deviceDimmerDown.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        cell.deviceOff.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        cell.isSelectedDevice.tag = indexPath.row
        cell.isSelectedDevice.addTarget(self, action: #selector(cellSelectedTapped(sender:)), for: .touchUpInside)

        //Selection background color
        let bgColorView = UIView()
        bgColorView.backgroundColor = .white
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    @objc func cellSelectedTapped(sender: UIButton) {
        let isDeviceSelected: Bool = devicesForThisScene[sender.tag]["isAdded"] as? Bool ?? true
        
        if isDeviceSelected {
            devicesForThisScene[sender.tag]["isAdded"] = true
            devicesForThisScene[sender.tag]["brightness"] = 0
            devicesForThisScene[sender.tag]["isOn"] = false
        } else {
            devicesForThisScene[sender.tag]["isAdded"] = true
            devicesForThisScene[sender.tag]["brightness"] = 100
            devicesForThisScene[sender.tag]["isOn"] = true
        }
        self.tableView.reloadData()

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch selectedIndexPath {
        case nil:
            selectedIndexPath = indexPath
            self.currentIndexPath = indexPath.row
        default:
            if selectedIndexPath! as IndexPath == indexPath {
                selectedIndexPath = nil
            } else {
                selectedIndexPath = indexPath
                self.currentIndexPath = indexPath.row
            }
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let smallHeight: CGFloat = 80.0
        let expandedHeight: CGFloat = 400
        let ip = indexPath
        if selectedIndexPath != nil {
            if ip == selectedIndexPath! {
                return expandedHeight
            } else {
                return smallHeight
            }
        } else {
            return smallHeight
        }
    }
    
    
}

