//
//  EditSceneVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/27/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class EditSceneVC: CoreBaseController, UpdateIconDelegate {
    
    @IBOutlet weak var sceneNameTF: UITextField!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var sceneIconImg: UIButton!
    @IBOutlet weak var devicesLbl: UIButton!
    @IBOutlet weak var devicesShadesAudioLbl: UIButton!
    
    var sceneName: String = ""
    var sceneIconName: String = ""
    var homeId: String = ""
    var devicesForThisScene = [[String: Any]]()
    var devices: [CasetaDevice] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
    }
    
    func setUpVisuals() {
        self.sceneNameTF.text = sceneName
        self.sceneNameTF.clearButtonMode = .always
        let iconImage = UIImage(named: sceneIconName)?.imageWithInsets(insets: UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10))?.withRenderingMode(.alwaysOriginal)
        self.sceneIconImg.setImage(iconImage, for: .normal)
        self.sceneIconImg.clipsToBounds = true
        self.sceneIconImg.contentMode = .scaleAspectFit
        self.sceneIconImg.layer.masksToBounds = true
    }
    
    func updateFirebase() {
        let db = Firestore.firestore()
        guard let newSceneName = sceneNameTF.text else { return }
        db.collection("scenes").whereField("name", isEqualTo: sceneName).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.updateData([
                "name" : newSceneName,
                "iconName" : self.sceneIconName
            ])
            self.performSegue(withIdentifier: "segueFromEditScenes", sender: nil)
        }
    }
    
    func updatedIconImage(newName: String) {
        self.sceneIconName = newName
        let iconImage = UIImage(named: newName)?.imageWithInsets(insets: UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10))?.withRenderingMode(.alwaysOriginal)
        self.sceneIconImg.setImage(iconImage, for: .normal)
        self.sceneIconImg.clipsToBounds = true
        self.sceneIconImg.contentMode = .scaleAspectFit
        self.sceneIconImg.layer.masksToBounds = true
    }
    
    func deleteFromFirebase() {
        let db = Firestore.firestore()
        
        db.collection("scenes").whereField("name", isEqualTo: sceneName).getDocuments { (snapshot, error) in
            let document = snapshot?.documents.first
            document?.reference.delete()
        }
        self.performSegue(withIdentifier: "segueFromEditScenes", sender: nil)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Success", message: "Scene has been updated!", alertType: .alert)
    }
    
    @IBAction func sceneIconPressed(_ sender: Any) {
        let sceneVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.PickANewSceneVC) as! PickANewSceneVC
        sceneVC.isChangingCurrentIcon = true
        sceneVC.updateIconDelegate = self
        let navController = UINavigationController(rootViewController: sceneVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }

    @IBAction func devicesPressed(_ sender: Any) {
        let setScenesVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SetSceneDefaultsVC) as! SetSceneDefaultsVC
        setScenesVC.isChangingRoomsForScene = true
        setScenesVC.devices = devices
//        setScenesVC.devicesForThisScene = devicesForThisScene
        setScenesVC.sceneName = sceneName
        setScenesVC.homeId = homeId
        let navController = UINavigationController(rootViewController: setScenesVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func deleteScenePressed(_ sender: Any) {
        self.displayAlertWithClosure(title: "Are you sure?", message: "Please confirm to delete this scene.", alertType: .actionSheet)
    }
    
    func displayAlertWithClosure(title: String, message: String, alertType: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.updateFirebase()
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.deleteFromFirebase()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
    
}
