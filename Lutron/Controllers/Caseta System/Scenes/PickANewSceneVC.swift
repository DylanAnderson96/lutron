//
//  PickANewSceneVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/23/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

protocol UpdateIconDelegate {
    func updatedIconImage(newName: String)
}

class PickANewSceneVC: CoreBaseController, UITextFieldDelegate {
    
    @IBOutlet weak var bellImg: UIImageView!
    @IBOutlet weak var lightbulbimg: UIImageView!
    @IBOutlet weak var candleImg: UIImageView!
    @IBOutlet weak var tvImg: UIImageView!
    @IBOutlet weak var cookingImg: UIImageView!
    @IBOutlet weak var eatingImg: UIImageView!
    @IBOutlet weak var drinkingImg: UIImageView!
    @IBOutlet weak var bedtimeImg: UIImageView!
    @IBOutlet weak var homeImg: UIImageView!
    @IBOutlet weak var morningImg: UIImageView!
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var musicImg: UIImageView!
    @IBOutlet weak var lightOutImg: UIImageView!
    @IBOutlet weak var airplaneModeImg: UIImageView!
    @IBOutlet weak var readingImg: UIImageView!
    @IBOutlet weak var chillingImg: UIImageView!
    @IBOutlet weak var sunriseImg: UIImageView!
    @IBOutlet weak var sunsetImg: UIImageView!
    
    @IBOutlet weak var scenenameTF: UITextField!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    
    var selectedIcon = UIImage()
    var devices: [CasetaDevice] = []
    var homeId: String = ""
    var imageName: String = ""
    var isChangingCurrentIcon: Bool = false
    var updateIconDelegate: UpdateIconDelegate?
    @IBOutlet weak var theTitle: UINavigationItem!
    
    @IBOutlet weak var stackViewWithImages: UIStackView!
    @IBOutlet weak var stackViewWithlables: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scenenameTF.delegate = self
        self.addTapGestureForAllImages()
        self.setUpVisuals()
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        if isChangingCurrentIcon {
            self.updateIconDelegate?.updatedIconImage(newName: self.imageName)
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setUpVisuals() {
        if isChangingCurrentIcon {
            self.stackViewWithlables.isHidden = true
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .clear
            self.theTitle.title = "Scene Icon"
        } else {
            self.theTitle.title = "Name & Icon"
            self.stackViewWithlables.isHidden = false
            self.stackViewWithImages.isHidden = false
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    func addTapGestureForAllImages() {
        let bellTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let lightTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let candleTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let tvTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let cookingTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let eatingTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let drinkingTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let bedTimeTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let homeTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let morningTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let movieTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let musicTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let lightsOutTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let airplaneModeTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let readingTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let chillingTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let sunriseTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))
        let sunsetTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(sender:)))

        self.bellImg.addGestureRecognizer(bellTapGesture)
        self.lightbulbimg.addGestureRecognizer(lightTapGesture)
        self.candleImg.addGestureRecognizer(candleTapGesture)
        self.tvImg.addGestureRecognizer(tvTapGesture)
        self.cookingImg.addGestureRecognizer(cookingTapGesture)
        self.eatingImg.addGestureRecognizer(eatingTapGesture)
        self.drinkingImg.addGestureRecognizer(drinkingTapGesture)
        self.bedtimeImg.addGestureRecognizer(bedTimeTapGesture)
        self.homeImg.addGestureRecognizer(homeTapGesture)
        self.morningImg.addGestureRecognizer(morningTapGesture)
        self.movieImg.addGestureRecognizer(movieTapGesture)
        self.musicImg.addGestureRecognizer(musicTapGesture)
        self.lightOutImg.addGestureRecognizer(lightsOutTapGesture)
        self.airplaneModeImg.addGestureRecognizer(airplaneModeTapGesture)
        self.readingImg.addGestureRecognizer(readingTapGesture)
        self.chillingImg.addGestureRecognizer(chillingTapGesture)
        self.sunriseImg.addGestureRecognizer(sunriseTapGesture)
        self.sunsetImg.addGestureRecognizer(sunsetTapGesture)

        self.bellImg.tag = 1
        self.lightbulbimg.tag = 2
        self.candleImg.tag = 3
        self.tvImg.tag = 4
        self.cookingImg.tag = 5
        self.eatingImg.tag = 6
        self.drinkingImg.tag = 7
        self.bedtimeImg.tag = 8
        self.homeImg.tag = 9
        self.morningImg.tag = 10
        self.movieImg.tag = 11
        self.musicImg.tag = 12
        self.lightOutImg.tag = 13
        self.airplaneModeImg.tag = 14
        self.readingImg.tag = 15
        self.chillingImg.tag = 16
        self.sunriseImg.tag = 17
        self.sunsetImg.tag = 18

    }
    
    @objc func imageViewTapped(sender: UITapGestureRecognizer) {
        self.bellImg.layer.shadowOpacity = 0.0
        self.lightbulbimg.layer.shadowOpacity = 0.0
        self.candleImg.layer.shadowOpacity = 0.0
        self.tvImg.layer.shadowOpacity = 0.0
        self.cookingImg.layer.shadowOpacity = 0.0
        self.eatingImg.layer.shadowOpacity = 0.0
        self.drinkingImg.layer.shadowOpacity = 0.0
        self.bedtimeImg.layer.shadowOpacity = 0.0
        self.homeImg.layer.shadowOpacity = 0.0
        self.morningImg.layer.shadowOpacity = 0.0
        self.movieImg.layer.shadowOpacity = 0.0
        self.musicImg.layer.shadowOpacity = 0.0
        self.lightOutImg.layer.shadowOpacity = 0.0
        self.airplaneModeImg.layer.shadowOpacity = 0.0
        self.readingImg.layer.shadowOpacity = 0.0
        self.chillingImg.layer.shadowOpacity = 0.0
        self.sunriseImg.layer.shadowOpacity = 0.0
        self.sunsetImg.layer.shadowOpacity = 0.0

        if let imageView = view.viewWithTag(sender.view?.tag ?? 1) as? UIImageView {
            imageView.applyShadow()
            self.selectedIcon = imageView.image!
            if sender.view?.tag == 1 {
                self.imageName = "bell"
            } else if sender.view?.tag == 2 {
                self.imageName = "lightbulb"
            } else if sender.view?.tag == 3 {
                self.imageName = "candle"
            } else if sender.view?.tag == 4 {
                self.imageName = "tv"
            } else if sender.view?.tag == 5 {
                self.imageName = "cooking"
            } else if sender.view?.tag == 6 {
                self.imageName = "eating"
            } else if sender.view?.tag == 7 {
                self.imageName = "drinking"
            } else if sender.view?.tag == 8 {
                self.imageName = "bedtime"
            } else if sender.view?.tag == 9 {
                self.imageName = "home"
            } else if sender.view?.tag == 10 {
                self.imageName = "morning"
            } else if sender.view?.tag == 11 {
                self.imageName = "movie"
            } else if sender.view?.tag == 12 {
                self.imageName = "music"
            } else if sender.view?.tag == 13 {
                self.imageName = "lightsOut"
            } else if sender.view?.tag == 14 {
                self.imageName = "airplaneMode"
            } else if sender.view?.tag == 15 {
                self.imageName = "reading"
            } else if sender.view?.tag == 16 {
                self.imageName = "chilling"
            } else if sender.view?.tag == 17 {
                self.imageName = "sunrise"
            } else if sender.view?.tag == 18 {
                self.imageName = "sunset"
            }
            
            updateIconDelegate?.updatedIconImage(newName: imageName)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.isEmpty ?? true {
            self.nextBtn.isEnabled = false
            self.nextBtn.tintColor = .darkGray
            return true
        } else {
            self.nextBtn.isEnabled = true
            self.nextBtn.tintColor = .link
            return true
        }
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        let setSceneDefaultsVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SetSceneDefaultsVC) as! SetSceneDefaultsVC
        setSceneDefaultsVC.selectedIcon = self.selectedIcon
        setSceneDefaultsVC.sceneName = String(scenenameTF.text!)
        setSceneDefaultsVC.devices = devices
        setSceneDefaultsVC.homeId = homeId
        setSceneDefaultsVC.imageName = imageName
        let navController = UINavigationController(rootViewController: setSceneDefaultsVC)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true)
    }
    

}
