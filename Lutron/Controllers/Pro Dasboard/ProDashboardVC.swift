//
//  ProDashboardVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class ProDashboardVC: CoreBaseController {

    struct ProDashboard {
        var cellHeader: String!
        var cellDescription: String!
    }
    
    @IBOutlet weak var profileBtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var stackViewPersonalAccount: UIStackView!
    @IBOutlet weak var switchToPersonalAccountBtn: UIButton!
    @IBOutlet weak var personalAccountEmail: UILabel!
    
    var contractor: User!
    var isDefaultScreenShowing: Bool = true
    var isContractorAuthenticated: Bool = false
    var contractorHasSignedIntoPersonalAccount: Bool = false
    var homeOwnerLastName: String = ""
    
    var proDashboardOptions: [ProDashboard] = []
    var proDashboardDefaultDict: [[String: String]] = [
        ["Sign In or Create Account": "Sign in with a LightKit account to access more features like loading previous client homes"],
        ["Contractor Mode": "Setup a LightKit system without an account"]
    ]
    
    var contractorLoginWithPossibleKetra: [[String: String]] = [
        ["Set Up a new system": "Configure a system and invite the homeowner."],
        ["Access an existing system": "Access a system locally or remotely with a code"],
        ["Activate Ketra devices": "Assign Ketra devices for a HomeWorks system"],
        ["Connect to Residence": "(home@owner.com)"]
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupVisuals()
    }
    
    func setupVisuals() {
        if isDefaultScreenShowing {
            self.profileBtn.isEnabled = false
            self.profileBtn.tintColor = .systemBackground
            self.dismissBtn.isEnabled = true
            self.dismissBtn.tintColor = .link
            self.stackViewPersonalAccount.isHidden = true
            self.proDashboardOptions.removeAll()
            
            for prodashboardOption in proDashboardDefaultDict {
                for (key, value) in prodashboardOption {
                    proDashboardOptions.append(ProDashboard(cellHeader: key, cellDescription: value))
                }
            }
        } else if isContractorAuthenticated {
            self.profileBtn.isEnabled = true
            self.profileBtn.tintColor = .link
            self.dismissBtn.isEnabled = false
            self.dismissBtn.tintColor = .systemBackground
            self.stackViewPersonalAccount.isHidden = false

            self.proDashboardOptions.removeAll()
            
            if contractor.connections.isEmpty {
                self.contractorLoginWithPossibleKetra.removeLast()
            } else {
                self.contractorLoginWithPossibleKetra[3] = ["Connect to \(String(contractor.connections.first!))'s residence": "(\(String(contractor.eMail)))"]
            }
            
            if !contractor.certifications.contains("Ketra") {
                self.contractorLoginWithPossibleKetra.remove(at: 2)
            }
                        
            for prodashboardOption in contractorLoginWithPossibleKetra {
                for (key, value) in prodashboardOption {
                    proDashboardOptions.append(ProDashboard(cellHeader: key, cellDescription: value))
                }
            }
        }
        
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        self.tableView.tableHeaderView?.backgroundColor = .black

        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: tableView.frame.maxY, width: tableView.frame.width, height: 1))
        self.tableView.tableFooterView?.backgroundColor = .black
        
        self.tableView.reloadData()
    }
    
    @IBAction func unwindToProDashboard(_ sender: UIStoryboardSegue) { }
        
    @IBAction func profileButtonPressed(_ sender: Any) {
        self.launchContractorAuth(isEditable: true, contractor: contractor)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismissDetail(transitionDirection: .fromLeft, isAnimated: false)
    }
    
    @objc func cellBtnSelected(sender: UIButton) {
        if isDefaultScreenShowing {
            if sender.tag == Constants.ProDashboardOptions.createAccount {
                self.launchProDashboardVC()
            } else if sender.tag == Constants.ProDashboardOptions.contractorMode {
                self.launchContractorMode()
            }
        } else if isContractorAuthenticated {
            if sender.tag == Constants.ProDashboardOptions.setUpNewSystem {
                self.displayAlert(title: "Add New System", message: "This would go through the flow of adding a new system.")
            } else if sender.tag == Constants.ProDashboardOptions.accessExistingSystem {
                self.displayAlert(title: "Access Existing System", message: "This would go through the flow of accessing an existing system.")
            } else if sender.tag == Constants.ProDashboardOptions.ketraActivation {
                self.displayAlert(title: "Add New System", message: "This would go through the flow of adding Ketra devices.")
            } else if sender.tag == Constants.ProDashboardOptions.connectToResidence {
                self.displayAlert(title: "Connect To Residence", message: "This would connect the contractor the the current residence.")
            }
        }
    }
    
    @IBAction func signIntoPersonalAccountPressed(_ sender: Any) {
        if contractorHasSignedIntoPersonalAccount {
            self.displayAlert(title: "Change Account", message: "This action would now sign the contractor into their personal account.")
        } else {
            contractorHasSignedIntoPersonalAccount = true
            self.switchToPersonalAccountBtn.setTitle("Switch to Personal Home Account", for: .normal)
            self.personalAccountEmail.isHidden = false
        }
    }
    
    func launchProDashboardVC() {
        let proDashboardVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.SignInProDashboardVC) as! SignInProDashboardVC
        proDashboardVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(proDashboardVC, animated: true)
    }
    
    func launchContractorMode() {
        let contractorModeVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.ContractorModeVC) as! ContractorModeVC
        contractorModeVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(contractorModeVC, animated: true)
    }
}

extension ProDashboardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return proDashboardOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "proDashboardCell", for: indexPath) as! ProDashboardCell
        cell.isUserInteractionEnabled = true
        cell.headerLbl.setTitle(proDashboardOptions[indexPath.row].cellHeader, for: .normal)
        cell.descriptionLbl.setTitle(proDashboardOptions[indexPath.row].cellDescription, for: .normal)
        cell.headerLbl.tag = indexPath.row
        cell.descriptionLbl.tag = indexPath.row
        cell.cellSelectedBtn.tag = indexPath.row
        cell.descriptionLbl.titleLabel?.numberOfLines = 2
        cell.descriptionLbl.titleLabel?.adjustsFontSizeToFitWidth = true
        cell.descriptionLbl.titleLabel?.lineBreakMode = .byWordWrapping
        cell.headerLbl.addTarget(self, action: #selector(cellBtnSelected(sender:)), for: .touchUpInside)
        cell.descriptionLbl.addTarget(self, action: #selector(cellBtnSelected(sender:)), for: .touchUpInside)
        cell.cellSelectedBtn.addTarget(self, action: #selector(cellBtnSelected(sender:)), for: .touchUpInside)
        if proDashboardOptions[indexPath.row].cellHeader.contains("Connect") {
            cell.headerLbl.setTitleColor(.darkBlue, for: .normal)
            cell.descriptionLbl.setTitleColor(.lightGray, for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isDefaultScreenShowing {
            if indexPath.row == Constants.ProDashboardOptions.createAccount {
                self.launchProDashboardVC()
            } else if indexPath.row == Constants.ProDashboardOptions.contractorMode {
                self.launchContractorMode()
            }
        } else if isContractorAuthenticated {
            if indexPath.row == Constants.ProDashboardOptions.setUpNewSystem {
                self.displayAlert(title: "Add New System", message: "This would go through the flow of adding a new system.")
            } else if indexPath.row == Constants.ProDashboardOptions.accessExistingSystem {
                self.displayAlert(title: "Access Existing System", message: "This would go through the flow of accessing an existing system.")
            } else if indexPath.row == Constants.ProDashboardOptions.ketraActivation {
                self.displayAlert(title: "Add New System", message: "This would go through the flow of adding Ketra devices.")
            } else if indexPath.row == Constants.ProDashboardOptions.connectToResidence {
                self.displayAlert(title: "Connect To Residence", message: "This would connect the contractor the the current residence.")
            }
        }
    }
}
