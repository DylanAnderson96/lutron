//
//  SignInProDashboardVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/16/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import FirebaseFirestoreSwift

class SignInProDashboardVC: CoreBaseController {

    @IBOutlet weak var emailAddressTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var createAccountBtn: UIButton!
    var users: [[String: Any]] = [[:]]
    var contractors: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.isNavigationBarHidden = true
        self.setUpVisual()
        self.users = Contractors.contractors
        for user in users {
            contractors.append(User(user))
        }
        
    }
    
    func setUpVisual() {
        self.signInBtn.layer.cornerRadius = 5
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        self.displayAlert(title: "Forgot Your Password?", message: "In production this would be a 'forgot password' view for user to go through the motion to reset password.")
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        guard let email = emailAddressTf.text else { return }
        guard let password = passwordTf.text else { return }
        
        if email.isEmpty || password.isEmpty {
            self.displayAlert(title: "Whoops", message: "Please enter an email and password to continue")
        } else {
            if self.isUserAuthenticated(email: email, password: password) {
                guard let currentContractor = self.getCurrentContractor(email: email) else { return }
                self.launchContractorAuth(isEditable: false, contractor: currentContractor)
            } else {
                self.displayAlert(title: "Whoops", message: "Please enter a valid email and password to continue.")
            }
        }
    }
    
    func getCurrentContractor(email: String) -> User? {
        for contractor in contractors {
            if email == contractor.eMail {
                return contractor
            }
        }
        return nil
    }
    
    func isUserAuthenticated(email: String, password: String) -> Bool {
        for contractor in contractors {
            if email == contractor.eMail {
                if password == contractor.password {
                    return true
                } else {
                    return false
                }
            }
        }
        return false
    }

    @IBAction func createAccountPressed(_ sender: Any) {
        self.displayAlert(title: "Create Account", message: "In production this would be a webview of the 'Create a New Account' from myLightKit.")
    }
    
}
