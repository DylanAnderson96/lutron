//
//  ListOfHomesVC.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/15/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

class ListOfHomesVC: CoreBaseController {
    
    var homes: [CasetaHome] = []
    let db = Firestore.firestore()
    @IBOutlet weak var tableView: UITableView!
    var homeId: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFirebaseHomes()
        
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
        self.tableView.tableHeaderView?.backgroundColor = .black

        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: tableView.frame.maxY, width: tableView.frame.width, height: 1))
        self.tableView.tableFooterView?.backgroundColor = .black

    }
    
    func getFirebaseHomes() {
        db.collection(Constants.FirestoreCollections.homes).addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("There was an error getting the data from Firebase \(String(describing: error?.localizedDescription))")
            } else {
                self.homeId.removeAll()
                self.homes.removeAll()

                for home in snapshot!.documents {
                    self.homes.append(CasetaHome(home.data()))
                    self.homeId.append(home.documentID)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func addNewHomePressed(_ sender: Any) {
        
    }
    
}

extension ListOfHomesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listOfHomesCell", for: indexPath) as! ListOfHomesCell
        cell.homeName.text = homes[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let casetaSystemVC = self.storyboard?.instantiateViewController(identifier: Constants.Segues.CasetaSystemView) as! CasetaSystemView
        casetaSystemVC.modalPresentationStyle = .fullScreen
        casetaSystemVC.homeId = homeId[indexPath.row]
        casetaSystemVC.homeName = homes[indexPath.row].name
        UserDefaults.standard.set(homeId[indexPath.row], forKey: "homeId")
        UserDefaults.standard.set(homes[indexPath.row].name, forKey: "homeName")
        self.present(casetaSystemVC, animated: true, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
}
