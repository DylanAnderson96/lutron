//
//  Users.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/20/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class Contractors {
    static var contractors: [[String: Any]] = [
        ["Company": "John Smith Inc.", "Phone": "(919) 222-5678", "E-mail": "smith@john.com", "Password": "pass1234", "Account":"224578", "Installer Name": "John Smith", "Certifications": [""], "Connections": []],
        ["Company": "Dylan Anderson Inc.", "Phone": "(919) 888-2132", "E-mail": "dylan@anderson.com", "Password": "pass5678", "Account":"123456", "Installer Name": "Dylan Anderson", "Certifications": ["Ketra"], "Connections": ["Smith"]],
        ["Company": "Ralph Anderson Inc.", "Phone": "(845) 958-2549", "E-mail": "ralph@anderson.com", "Password": "pass9101", "Account":"987654", "Installer Name": "Ralph Anderson", "Certifications": ["Ketra"], "Connections": ["Williams"]],
        ["Company": "EASi", "Phone": "(975) 0870-2256", "E-mail": "easi@easi.com", "Account":"555888", "Password": "pass1121", "Installer Name": "EASi", "Certifications": ["Ketra"], "Connections": []],
        ["Company": "Tyler Troung Inc.", "Phone": "(875) 875-9987", "E-mail": "tyler@troung.com", "Password": "pass4151", "Account":"777888", "Installer Name": "Tyler Troung", "Certifications": [""], "Connections": ["Wilson"]]
    ]
}
