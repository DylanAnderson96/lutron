//
//  Constants.swift
//  Lutron
//
//  Created by Dylan Anderson on 1/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class Constants {
    
    struct Segues {
        static let InitialVC = "InitialVC"
        static let segueToOnboardingVC = "segueToOnboardingVC"
        static let OnboardingScreenOne = "OnboardingScreenOne"
        static let SignInVC = "SignInVC"
        static let CasetaSystemView = "CasetaSystemView"
        static let RA2SelectSystemView = "RA2SelectSystemView"
        static let HomeWorksSystemView = "HomeWorksSystemView"
        static let ProDashboardVC = "ProDashboardVC"
        static let ListOfHomesVC = "ListOfHomesVC"
        static let SignInProDashboardVC = "SignInProDashboardVC"
        static let ContractorAuthenticationVC = "ContractorAuthenticationVC"
        static let ContractorModeVC = "ContractorModeVC"
        static let SettingsVC = "SettingsVC"
        static let AddNewDeviceVC = "AddNewDeviceVC"
        static let SelectRoomForDeviceVC = "SelectRoomForDeviceVC"
        static let PairDevice = "PairDevice"
        static let EditDeviceVC = "EditDeviceVC"
        static let InstallNewDevices = "InstallNewDevices"
        static let PickANewSceneVC = "PickANewSceneVC"
        static let SetSceneDefaultsVC = "SetSceneDefaultsVC"
        static let EditSceneVC = "EditSceneVC"
        static let DaysOfWeekScheduleVC = "DaysOfWeekScheduleVC"
        static let ScheduleTimeAndDate = "ScheduleTimeAndDate"
        static let ScheduleNameVC = "ScheduleNameVC"
        static let EditScheduleVC = "EditScheduleVC"
    }
    
    struct SegmentControlState {
        static let devices = 0
        static let scenes = 1
        static let schedules = 2
    }
    
    struct FirestoreCollections {
        static let devices = "devices"
        static let homes = "homes"
        static let rooms = "rooms"
        static let scenes = "scenes"
        static let schedules = "schedules"
    }
    
    struct DeviceState {
        static let ON = 1
        static let OFF = 0
    }
    
    struct ProDashboardOptions {
        static let createAccount = 0
        static let contractorMode = 1
        static let setUpNewSystem = 0
        static let accessExistingSystem = 1
        static let ketraActivation = 2
        static let connectToResidence = 3
    }
    
    struct User {
        static let company = "Company"
        static let phone = "Phone"
        static let eMail = "E-mail"
        static let account = "Account"
        static let installerName = "Installer Name"
        static let certifications = "Certifications"
        static let password = "Password"
    }
}



